<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
# Stephane Casset, Raphael Hertzog, Logidee 2000-2001
# See LICENSE file for copyright notice
-->

<!-- DTD for training documents -->

<!-- Entities -->
<!ENTITY nbsp  "&#x00A0;">
<!ENTITY tir   "&#x2014;">
<!ENTITY reg   "&#x00A9;">
<!ENTITY quot  "&#x0022;">
<!ENTITY ctrl  "^">
<!ENTITY OElig "&#x0152;">
<!ENTITY oelig "&#x0153;">
<!ENTITY euro  "&#x20AC;">

<!-- Entities for XML compliance -->
<!ENTITY lt     "&#38;#60;">
<!ENTITY gt     "&#62;">
<!ENTITY amp    "&#38;#38;">
<!ENTITY apos   "&#39;">
<!ENTITY quot   "&#34;">

<!-- Old entities for compatibilty only, don't use them -->
<!ENTITY ud    "_">
<!ENTITY di    "#">
<!ENTITY dol   "$">
<!ENTITY ti    "~">
<!ENTITY pc    "&#x0025;">
<!ENTITY bs    "\">
<!ENTITY aco   "{">
<!ENTITY acf   "}">
<!ENTITY circ  "^">
<!ENTITY et    "&amp;">

<!-- Elements -->
<!ELEMENT formation (info,(xi:include|include|theme)+)>
<!ELEMENT theme (info,(xi:include|include|module)+)>
<!ELEMENT module (info,page+)>
<!ELEMENT slideshow ((info|shortinfo),slide+)>

<!ELEMENT include EMPTY>
<!ATTLIST include href CDATA #REQUIRED>

<!-- Text attributes -->
<!ENTITY % text "#PCDATA">

<!ELEMENT em (%text;)>
<!ELEMENT code (%text;)>
<!ATTLIST code
  visible (true|false) "false">

<!ELEMENT menu    (%text;)>
<!ELEMENT cmd     (%text;)>
<!ELEMENT file    (%text;)>

<!ELEMENT url     (%text;)>
<!ATTLIST url
  href CDATA #REQUIRED>

<!ENTITY % text_attribute "em|code|cmd|menu|file|url">
<!ENTITY % container "%text;|%text_attribute;|list|table|image|math|glossary">

<!-- The info header -->

<!ELEMENT info (title,ref,description,objectives,ratio,duration,prerequisite?,dependency?,suggestion?,version+,level,state,proofreaders?)>
<!ELEMENT shortinfo (title,description,version+)>
<!ELEMENT title (%text;|%text_attribute;)*>
<!ELEMENT ref (%text;)>
<!ELEMENT description (para+)>
<!ELEMENT objectives (item+)>
<!ELEMENT ratio EMPTY>
<!ATTLIST ratio
  value CDATA #REQUIRED>
<!ELEMENT duration EMPTY>
<!ATTLIST duration 
  value CDATA #REQUIRED
  unit  CDATA #REQUIRED>
<!ELEMENT prerequisite (para*)>
<!ELEMENT dependency (ref*)>
<!ELEMENT suggestion (ref*)>
<!ELEMENT version (author,email?,comment,date?)>
<!ATTLIST version
  number CDATA #REQUIRED>
<!ELEMENT author  (%text;)>
<!ELEMENT comment (%text;)>
<!ELEMENT email   (%text;)>
<!ELEMENT date    (%text;)>
<!ELEMENT level EMPTY>
<!ATTLIST level
  value CDATA #REQUIRED>
<!ELEMENT state EMPTY>
<!ATTLIST state
  finished    (true|false) #REQUIRED
  proofread   (true|false) #REQUIRED
>
<!ELEMENT proofreaders (item*)>

<!-- The page -->

<!ELEMENT page (title,(slide|para|note)*,(section,(slide|section)*)?,exercise*)>
<!ATTLIST page 
  restriction CDATA "all" >

<!-- The optionnal slide -->

<!ELEMENT slide (title,(subtitle?,para)+)>
<!ATTLIST slide 
  background CDATA #IMPLIED>

<!ELEMENT subtitle (%text;|%text_attribute;)*>

<!-- The content of the page (associated with the slide) -->

<!ELEMENT section (title,(section|para|note|exercise|slide)+)>
<!ATTLIST section 
  restriction CDATA "all" >

<!ELEMENT para (%container;|note)*>
<!ATTLIST para 
  icon        CDATA #IMPLIED
  restriction CDATA "all" >

<!ELEMENT caption (%container;)*>

<!ELEMENT glossary (%container;)*>
<!ATTLIST glossary
  name CDATA #REQUIRED >

<!ELEMENT list (item|list)+>
<!ELEMENT item (%container;|para)*>

<!ELEMENT note (%container;|para)*>
<!ATTLIST note 
  trainer     (true|false) "false"
  icon        CDATA #IMPLIED
  restriction CDATA "all" >

<!ELEMENT image (caption?)>
<!ATTLIST image 
  src     CDATA #REQUIRED
  visible (true|false) "false"
  scale   CDATA "0" >

<!-- Exercises -->

<!ELEMENT exercise (title?,question,answer?)>
<!ATTLIST exercise 
  icon        CDATA #IMPLIED
  restriction CDATA "all" >
<!ELEMENT question (para+)>
<!ELEMENT answer   (para+)>

<!-- Tables -->

<!ELEMENT table (row+)>
<!ATTLIST table 
  border (0|1) "1">
<!ELEMENT row    (col+)>
<!ATTLIST row
  border (0|1)   "1">
<!ELEMENT col (%container;)*>
<!ATTLIST col
  border (0|1)   #IMPLIED 
  align  (c|l|r) #IMPLIED
  head   (0|1)   "0">

<!-- Math notation -->

<!ELEMENT math (#PCDATA)>
<!ATTLIST math
  notation (tex|html) #REQUIRED>

