" Fichier de syntaxe XML pour Logidée, basé sur le file de Syntaxe XML
" standard de vim et les macros de Marc Simon

" <BS> = 2 espaces en arrière, <TAB> = 2 espaces en avant
set sts=2
set sw=2

"if exists("b:did_ftplugin")
"    finish
"endif

" Don't load another plugin for this buffer
" let b:did_ftplugin = 1

"
" Macros
"

imap ,pag <page><CR><title></title><CR><CR></page><CR><ESC>3kf>a
imap ,ti <title></title><ESC>F>a
imap ,ta <table><CR>  <CR><BS></table><ESC>ka
imap ,ro <row><CR>  <CR><BS></row><ESC>ka
imap ,it <item></item><ESC>F>a
imap ,ref <ref></ref><ESC>F>a
imap ,ob <objectives><CR>  <CR><BS></objectives><ESC>ka
imap ,et <state finished="" proofread="false"/><ESC>3F"i
imap ,de <dependency></dependency><ESC>F>a
imap ,su <suggestion></suggestion><ESC>F>a
imap ,au <author></author><ESC>F>a
imap ,ve <version number=""><CR><author></author><CR><comment></comment><CR><BS></version><ESC>3kf"a
imap ,in <info><CR>  <title></title><CR><ref></ref><CR><description><CR><para></para><CR></description><CR><objectives><CR>  <item></item><CR><BS></objectives><CR><ratio value="th/pr"/><CR><duration value="" unit=""/><CR><!--<prerequisite></prerequisite><CR><dependency><CR>  <ref></ref><CR><BS></dependency><CR><suggestion><CR>  <ref></ref><CR><BS></suggestion>--><CR><version number="1.0"><CR>  <author></author><CR><comment></comment><CR><BS></version><CR><level value=""/><CR><state finished="false" proofread="false"/><CR><!--<proofreaders><CR></proofreaders>--><CR><BS></info>
imap ,se <section><CR><title></title><CR><para></para><CR></section><ESC>2kF>a
imap ,sl <slide><CR><title></title><CR></slide><CR><ESC>kkf>a
imap ,par <para></para><ESC>F>a
imap ,em <em></em><ESC>F>a
imap ,cod <code></code><ESC>F>a
imap ,no <note></note><ESC>F>a
imap ,ex <exercise></exercise><ESC>F>a
imap ,qu <question></question><ESC>F>a
imap ,an <answer></answer><ESC>F>a
imap ,li <list><CR><item></item><CR><item></item><CR></list><CR><ESC>kkkf>a
imap ,col <col></col><ESC>F>a
imap ,fi <file></file><ESC>F<i
imap ,cm <cmd></cmd><ESC>F<i
imap ,me <menu></menu><ESC>F<i
imap ,im <image src=""/><ESC>F"i
imap ,uh <url href="http://"></url><ESC>F"i
imap ,ur <url href="http://"></url><ESC>F"i
imap ,uf <url href="ftp://"></url><ESC>F"i
imap ,mo <module><CR><CR></module><ESC>ki
imap ,glo <glossary name=""></glossary><ESC>F"i
imap ,gls <glossary name=""/><ESC>F"i
imap ,su <subtitle></subtitle><ESC>F<i
imap &n &nbsp;
imap « «&n&n»<ESC>2F;a

imap ,xm <?xml version="1.0" encoding="UTF-8" standalone="no" ?><CR>
imap ,dt <!DOCTYPE module PUBLIC "-//Logidee//DTD logidee-tools V1.2//EN" "../dtd/module.dtd"><CR>
imap ,fo <formation xmlns:xi="http://www.w3.org/2001/XInclude"><CR>  ,in<CR><BS></formation><ESC>ko
imap ,th <theme xmlns:xi="http://www.w3.org/2001/XInclude"><CR>  ,in<CR><BS></theme><ESC>ko
imap ,xi <xi:include href=""/><ESC>F"i
imap ,com <!--  --><ESC>F i

vmap ,com <ESC>'<<HOME>i<!--<ESC>'><END>a--><ESC>

" vim: ts=8
