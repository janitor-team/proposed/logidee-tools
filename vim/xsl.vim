"
" Macros pour XSLT
"

set sts=2
set sw=2

imap ,xte <xsl:template match=""><CR>  <CR><BS></xsl:template><ESC>kkf"a
imap ,xtx <xsl:text></xsl:text><ESC>F<i
imap ,xch <xsl:choose><CR>  <xsl:when test=""><CR>  <CR><BS></xsl:when><CR><xsl:otherwise><CR>  <CR><BS></xsl:otherwise><CR><BS></xsl:choose><ESC>6kf"a
imap ,xi <xsl:if test=""><CR>  <CR><BS></xsl:if><ESC>kkf"a
imap ,xaa <xsl:apply-templates/>
imap ,xas <xsl:apply-templates select=""/><ESC>F"i
imap ,xam <xsl:apply-templates mode=""/><ESC>F"i
imap ,xva <xsl:variable name="" select=""/><ESC>3F"i
imap ,xe <xsl:element name=""><CR>  <CR><BS></xsl:element><ESC>kkf"a
imap ,xat <xsl:attribute name=""><CR>  <CR><BS></xsl:attribute><ESC>kkf"a
imap ,xs <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><CR><CR></xsl:stylesheet><ESC>ka
imap ,xm <?xml version="1.0" encoding="UTF-8" ?><CR>
imap ,xo <xsl:output encoding="UTF-8" method=""/><CR><ESC>k3f"a
imap ,xp <xsl:param name="" select="''"/><CR><ESC>kf"a
imap ,xf <xsl:for-each select=""><CR>  <CR><BS></xsl:for-each><ESC>kkf"a
imap ,xca <xsl:call-template name=""><CR>  <CR><BS></xsl:call-template><ESC>kkf"a
imap ,xw <xsl:with-param name=""></xsl:with-param><ESC>F"i
imap ,xn <xsl:number level="any" count="" format="1"/><ESC>3F"i
imap ,xvl <xsl:value-of select=""/><ESC>F"i
imap ,xd <xsl:document href="" method="" encoding="UTF-8"><CR>  <CR><BS></xsl:document>

" vim: ts=8
