%% This file is part of logidee-tools
%% http://www.logidee.com/
%%
%% See the LICENSE file for the copyright notice
%%
%% If you change this file, please keep the acknowledgement
%% of the work of Logidee and the other persons involved

% Define the messages for your language

\DeclareOption{fr}{%
\selectlanguage{french}
\def\Sommaire{Sommaire}
\def\Glossary{Glossaire}
\def\Exercices{Exercices}
\def\Exercice{Exercice}
\def\Solutions{Solutions}
\def\Reponse{Réponse~: }
\def\Reponsepage{Réponse page~}
\def\Objectives{Objectifs}
\def\Description{Description}
\def\Theme{Thème}
\def\Copy{Reproduction interdite.}
\def\Support{Support de cours}
\def\Auteurs{Auteurs~: }
\def\Avertissement{%
La reproduction dans ce manuel de noms de marques déposées, de désignations des
marques, etc. même sans stipulation explicite que de tels noms et marques sont
protégés par la loi, n'autorise pas à supposer qu'ils soient libres et puissent
être utilisés par quiconque.}% 
\def\Remerciements{%
Cet ouvrage a été mis en page avec \LaTeX, à partir de fichiers au format XML
convertis à l'aide de xsltproc développé par {\em Daniel Veillard}
({\tt http://xmlsoft.org/XSLT/}). Le logo TuX a été réalisé avec {\em The Gimp} par
{\em Larry Ewing}.}
\def\Logidoc{%
Les feuilles de styles, la DTD et les outils associés ont été developpés par
{\em Logidée} ({\tt http://www.logidee.com}) pour rédiger et maintenir ses
supports de formations. Ces outils sont bien sûr sous licence libre et
disponibles sur le site web mentionné ci-dessus.}%
\def\Droits{Tous droits réservés, reproduction interdite.}
\def\Legal{Toute représentation ou reproduction, intégrale ou partielle,
faite sans le consentement de l'auteur, ou de ses ayants cause, est illicite
(loi du 11 mars 1957, alinéa 1er de l'article 40). Cette représentation ou
reproduction, par quelque procédé que ce soit, constituerait une contrefaçon
sanctionnée par les articles 425 et suivants du Code pénal. La loi du 11 mars
1957 n'autorise, aux termes des alinéas 2 et 3 de l'article 41, que les copies
ou reproductions strictement réservées à l'usage privé du copiste et non
destinées à une utilisation collective d'une part, et, d'autre part, que les
analyses et les courtes citations dans un but d'exemple et d'illustration.}
}% français

\DeclareOption{en}{%
\selectlanguage{english}
\def\Sommaire{Table of Contents}
\def\Glossary{Glossary}
\def\Exercices{Exercises}
\def\Exercice{Exercise}
\def\Solutions{Solutions}
\def\Reponse{Solution: }
\def\Reponsepage{Solution page~}
\def\Objectives{Objectives}
\def\Description{Description}
\def\Theme{Theme}
\def\Copy{All rights reserved.}
\def\Support{Training Manual}
\def\Auteurs{Authors~: }
\def\Avertissement{%
The representation of registered names, trade names, the naming of goods, etc. in this training manual does not give the right, also where not specifically stipulated, to assume that such names, in terms of trade names or protection of trade name legislation, can be regarded as free and thus put to use  by anybody whatsoever.}% 
\def\Remerciements{%
This document was formatted with \LaTeX, from XML files converted with {\tt xsltproc} developed by {\em Daniel Veillard} ({\tt http://xmlsoft.org/XSLT/}). The Tux logo was made by {\em Larry Ewing} with {\em The Gimp}.}
\def\Logidoc{%
The style sheets, the DTD and the associated tools were developed by {\em
Logidée} ({\tt http://www.logidee.com}) for writing and maintaining training
manuals. These tools are provided under an Open Source Licence on the web site
mentioned above.}%

\def\Droits{All rights reserved.}
\def\Legal{This work is protected by copyright. All rights reserved for reproduction or copying of this training manual or parts thereof. This also applies to its translations. No parts of this work may, in any form whatsoever, (print, photocopy, microfilm or any other procedures), including for training purpose, be reproduced or electronically processed, duplicated or disseminated without the written permission of the publisher.}
}% english

\DeclareOption{de}{%
\selectlanguage{german}
%\usepackage[german]{babel}
\def\Sommaire{Inhaltsverzeichnis}
\def\Glossary{Glossar}
\def\Exercices{Übungen}
\def\Exercice{Übung}
\def\Solutions{Lösungen}
\def\Reponse{Lösung: }
\def\Reponsepage{Lösung Seite~}
\def\Objectives{Zielsetzungen}
\def\Description{Beschreibung}
\def\Theme{Thema}
\def\Copy{Alle Rechte vorbehalten.}
\def\Support{Trainings Handbuch}
\def\Auteurs{Author~: }
\def\Avertissement{%

Die Darstellung von registrierten Namen, Trademarks, Produktnamen, etc. gibt
keinerlei Rechte über diese; noch sollte deren Darstellung, sofern nicht anders
explizit angegeben, zur Annahme führen, daß diese Namen für Trademarks oder
Produktnamen frei und zur Benutzung durch dritte freigegeben wären.
}% 

\def\Remerciements{%
Dieses Dokument wurde mit \LaTeX formatiert. Diese Seiten wurden aus XML Dateien unter zu Hilfenahme von  {\tt xsltproc}
aufgebaut. {\tt xsltproc} wurde von {\em Daniel Veillard} ({\tt http://xmlsoft.org/XSLT/}) geschrieben. Das Tux-Logo wurde von {\em Larry Ewing} unter zu Hilfenahme von {\em The Gimp} erstellt.
}
\def\Logidoc{%
Die style sheets, die DTD und die zugehörigen Werkzeuge wurden von {\em
Logidée} ({\tt http://www.logidee.com}) entwickelt. Dieses System dient zur
Erstellung und Verwaltung von Kursunterlagen. Diese Werkzeuge stehen als Freie
Software auf oben gennannter Website zum herunterladen bereit.
}%

\def\Droits{Alle Rechte vorbehalten.}
\def\Legal{ Diese Arbeit ist durch das Urheberrecht geschützt. Alle Rechte zur
Verfielfältigung oder Kopien sind hiervon betroffen. Dies Betrifft auch
eventuelle Übersetzungen. Keine Teile dieses Dokuments dürfen, ohne
schriftlicher Einwilligung des Herausgebers, in welcher Form auch immer
(gedruckt, Fotokopie, Microfilm oder anderer Techniken), einschließlich zu
Ausbildungszwecken, verfielfältigt oder elektronisch verarbeitet, dupliziert
oder verteilt werden.  }
}% german


\DeclareOption{es}{%
\selectlanguage{spanish}
\def\Sommaire{Tabla de Contenidos}
\def\Glossary{Glosario}
\def\Exercices{Ejercicios}
\def\Exercice{Ejercicio}
\def\Solutions{Soluciones}
\def\Reponse{Solución: }
\def\Reponsepage{Pàgina de Soluciones:~}
\def\Objectives{Objetivos}
\def\Description{Descripción}
\def\Theme{Tema}
\def\Copy{Todos los derechos reservados.}
\def\Support{Manual de Formación}
\def\Auteurs{Autores~: }
\def\Avertissement{%
La representación de nombres y marcas registradas, etc. en este manual de formación no implica el derecho, donde no está especificamente estipulado, a asumir que dichos nombres puedan ser tratados como gratuítos y por tanto puestos a la disposición de cualquiera bajo ninguna circunstancia.}

\def\Remerciements{%
Este documento ha sido formateado con \LaTeX, desde ficheros XML convertidos con {\tt xsltproc} desarrollado por {\em Daniel Veillard} ({\tthttp://xmlsoft.org/XSLT/}). La imagen de Tux ha sido realizada por {\em Larry Ewing} utilizando {\em The Gimp}.}    
\def\Logidoc{%
Las hojas de estilo, DTD y herramientas asociadas han sido desarrroladas por {\em Logidée} ({\tt http://www.logidee.com}) para escribir y mantenar manuales de formación. Estas herramientas están disponibles bajo una licencia de software abierto en el sitio web mencionado arriba.}

\def\Droits{Todos los derechos resevados.}
\def\Legal{
Este trabajo está protegido por leyes de copyright. Todos los derechos reservaod para reproducción y copio de este manual de formación o partes del mismo. Esto también se aplica a traducciones del mismo. Ninguna parte de este trabajo debe ser reproducidas ó diseminadas por ningún tipo de medio (imprenta, fotocopia, microficha o cualquier otra técnica), incluídas razones de formación, sin el permiso escrito del editor.}
}% spanish

