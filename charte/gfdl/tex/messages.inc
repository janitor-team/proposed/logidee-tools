%% This file is part of logidee-tools
%% http://www.logidee.com/
%%
%% See the LICENSE file for the copyright notice
%%
%% If you change this file, please keep the acknowledgement
%% of the work of Logidee and the other persons involved

% Define your messages for your language

\DeclareOption{fr}{%
\selectlanguage{french}
\def\Sommaire{Sommaire}
\def\Glossary{Glossaire}
\def\Exercices{Exercices}
\def\Exercice{Exercice}
\def\Solutions{Solutions}
\def\Reponse{Réponse~: }
\def\Reponsepage{Réponse page~}
\def\Objectives{Objectifs}
\def\Description{Description}
\def\Theme{Thème}
\def\Copy{Ce document est sous Licence de Documentation Libre GNU.}
\def\Support{Support de cours}
\def\Auteurs{Auteurs~: }
\def\Avertissement{%
La reproduction dans ce manuel de noms de marques déposées, de désignations des
marques, etc. même sans stipulation explicite que de tels noms et marques sont
protégés par la loi, n'autorise pas à supposer qu'ils soient libres et puissent
être utilisés par quiconque.}% 
\def\Remerciements{%
Cet ouvrage a été mis en page avec \LaTeX, à partir de fichiers au format XML
convertis à l'aide de xsltproc développé par {\em Daniel Veillard}
({\tt http://xmlsoft.org/XSLT/}).}
\def\Logidoc{%
Les feuilles de styles, la DTD et les outils associés ont été developpés par
{\em Logidée} ({\tt http://www.logidee.com}) pour rédiger et maintenir ses
supports de formations. Ces outils sont bien sûr sous licence libre et
disponibles sur le site web mentionné ci-dessus.}%
\def\Droits{Permission est accordée de copier, distribuer et/ou modifier ce
document selon les termes de la Licence de Documentation Libre GNU (GNU Free
Documentation License disponible à {\tt http://www.gnu.org/copyleft/fdl.html}),
version 1.1 ou toute version ultérieure publiée par la Free Software
Foundation.}
\def\Legal{Veuillez vous référer aux sources XML de ce document (que vous
pouvez obtenir auprès de l'auteur) pour connaitre le détail de la licence avec
la liste des Sections Invariables, des Textes de Première de Couverture, et des
Textes de Quatrième de Couverture.}
}% français

\DeclareOption{en}{%
\selectlanguage{english}
\def\Sommaire{Table of Contents}
\def\Glossary{Glossary}
\def\Exercices{Exercises}
\def\Exercice{Exercise}
\def\Solutions{Solutions}
\def\Reponse{Solution: }
\def\Reponsepage{Solution page~}
\def\Objectives{Objectives}
\def\Description{Description}
\def\Theme{Theme}
\def\Copy{This document is under the GNU Free Documentation License.}
\def\Support{Training Manual}
\def\Auteurs{Authors~: }
\def\Avertissement{%
The representation of registered names, trade names, the naming of goods, etc. in this training manual does not give the right, also where not specifically stipulated, to assume that such names, in terms of trade names or protection of trade name legislation, can be regarded as free and thus put to use by anybody whatsoever.}% 
\def\Remerciements{%
This document was formatted with \LaTeX, from XML files converted with {\tt xsltproc} developed by {\em Daniel Veillard} ({\tt http://xmlsoft.org/XSLT/}). The Tux logo was made by {\em Larry Ewing} with {\em The Gimp}.}
\def\Logidoc{%
The style sheets, the DTD and the associated tools were developed by {\em
Logidée} ({\tt http://www.logidee.com}) for writing and maintaining training
manuals. These tools are provided under an Open Source Licence on the web site
mentioned above.}%
\def\Droits{Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation (GNU Free
Documentation License {\tt http://www.gnu.org/copyleft/fdl.html}).}
\def\Legal{Please see the XML files of this documentation (which you can
get from the author) to know the Invariant Sections, the Front-Cover Texts
and the Back-Cover Texts.}
}% english


\DeclareOption{de}{%
\selectlanguage{german}
%\usepackage[german]{babel}
\def\Sommaire{Inhaltsverzeichnis}
\def\Glossary{Glossar}
\def\Exercices{Übungen}
\def\Exercice{Übung}
\def\Solutions{Lösungen}
\def\Reponse{Lösung: }
\def\Reponsepage{Lösung Seite~}
\def\Objectives{Zielsetzungen}
\def\Description{Beschreibung}
\def\Theme{Thema}
\def\Copy{Alle Rechte vorbehalten.}
\def\Support{Trainings Handbuch}
\def\Auteurs{Author~: }
\def\Avertissement{%

Die Darstellung von registrierten Namen, Trademarks, Produktnamen, etc. gibt
keinerlei Rechte über diese; noch sollte deren Darstellung, sofern nicht anders
explizit angegeben, zur Annahme führen, daß diese Namen für Trademarks oder
Produktnamen frei und zur Benutzung durch dritte freigegeben wären.
}% 

\def\Remerciements{%
Dieses Dokument wurde mit \LaTeX formatiert. Diese Seiten wurden aus XML Dateien unter zu Hilfenahme von  {\tt xsltproc}
aufgebaut. {\tt xsltproc} wurde von {\em Daniel Veillard} ({\tt http://xmlsoft.org/XSLT/}) geschrieben. Das Tux-Logo wurde von {\em Larry Ewing} unter zu Hilfenahme von {\em The Gimp} erstellt.
}
\def\Logidoc{%
Die style sheets, die DTD und die zugehörigen Werkzeuge wurden von {\em
Logidée} ({\tt http://www.logidee.com}) entwickelt. Dieses System dient zur
Erstellung und Verwaltung von Kursunterlagen. Diese Werkzeuge stehen als Freie
Software auf oben gennannter Website zum herunterladen bereit.
}%

\def\Droits{Die Erlaubnis ist erteilt dieses Dokument zu kopieren, verteilen und/oder zu verändern unter den Aussagen der
GNU Free Documentation License ({\tt http://www.gnu.org/copyleft/fdl.html}), Version 1.1 oder einer beliebigen späteren Version welche von der
Free Software Foundation veröffentlicht werden sollte.}
\def\Legal{ Bitte entnehmen sie dem XML-Source dieses Dokuments (welchen Sie vom Author anfordern können) um die Details über die Lizenz bezüglich der invarianten Sektion, Deckblatt -und Rückentext wahrzunehmen.}
}% deutsch

\DeclareOption{es}{%
\selectlanguage{spanish}
\def\Sommaire{Tabla de Contenidos}
\def\Glossary{Glosario}
\def\Exercices{Ejercicios}
\def\Exercice{Ejercicio}
\def\Solutions{Soluciones}
\def\Reponse{Solución~: }
\def\Reponsepage{Pàgina de Soluciones~:~}
\def\Objectives{Objetivos}
\def\Description{Descripción}
\def\Theme{Tema}
\def\Copy{Este documento está bajo la Licencia de Documentación Libre GNU .}
\def\Support{Soporte del Curso}
\def\Auteurs{Autores~: }
\def\Avertissement{%
      El uso de marcas registradas, aun cuando implícito, no se ha de considerar público, ni puede ser, por tanto, usado por nadie.
}% 
\def\Remerciements{%
      Este documento ha sido creado con \LaTeX, a partir de ficheros en 
formato XML covertidos con la ayuda de xsltproc, desarrolado por {\em Daniel Veillard} ({\tt http://xmlsoft.org/XSLT/}).}
\def\Logidoc{%
      Las hojas de estilos, DTDs y utilidades asociadas han sido desarrolladas por {\em Logidée} ({\tt http://www.logidee.com}) para la creación y mantenimiento de
 manuales de formación. Estas herramientas están disponibles bajo una licencia  Open Source en el sitio web previamente mencionado.}%
\def\Droits{Se permite la copia, distribución y/o modificación de este documento según los términos de la Licencia de Documentación Libre GNU (GNU Free Documentation License disponible en Documentation License disponible à {\tt http://www.gnu.org/copyleft/fdl.html}), versión 1.1 o cualquier versión posterior publicada por la Free Software Foundation.}
\def\Legal{Refiérase al código fuente en XML de este documento (que puede ser obtenido contactando con el autor) para conocer las Secciones Invariables, los textos de la Portada y de la Contraportada.}
}% español

