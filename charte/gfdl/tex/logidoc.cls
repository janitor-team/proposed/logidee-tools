%% This file is part of logidee-tools
%% http://www.logidee.com/
%%
%% $Id$
%%
%% See the LICENSE file for the copyright notice
%%
%% If you change this file, please keep the acknowledgement
%% of the work of Logidee and the other persons involved

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{logidoc}
              [2000/19/04 v1.0
 Standard Default logidee-tools document class]
\usepackage{color}
\usepackage{ifthen}
\usepackage{fancyhdr}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{pstricks}
\usepackage{alltt}
\usepackage{verbatim}
\usepackage{pifont}
\usepackage{everyshi}
\usepackage{float}
\usepackage{eurosym}

\usepackage[english,french,german]{babel}

\usepackage{ucs}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{courier}

%\newrgbcolor{MyBlue}{0 0 0.54}
%\definecolor{MyBlue}{rgb}{0,0,0.54}
\newrgbcolor{LogideeBlue}{0.043 0.212 0.710 }
\definecolor{LogideeBlue}{rgb}{0.043,0.212,0.710}

% Include localized messages
\input messages.inc

\newboolean{presentation}
\newlength{\largeurtitre}
\newlength{\hauteurtitre}
\newlength{\hauteurlogo}
\newlength{\hauteurdiapototale}
\newlength{\hauteurdiapoclip}
\newlength{\hauteurdiapotitre}
\newlength{\hauteurdiapo}
\newlength{\hauteurimage}
\newlength{\hauteurimagedia}
\newlength{\footeroffset}
\newlength{\tmp}

\newcommand\@ptsize{}
\if@compatibility
  \renewcommand\@ptsize{0}
  \input{size10.clo}
\else
\DeclareOption{10pt}{\renewcommand\@ptsize{0}\input{size10.clo}}
\fi
\DeclareOption{11pt}{\renewcommand\@ptsize{1}\input{size11.clo}}
\DeclareOption{12pt}{\renewcommand\@ptsize{2}\input{size12.clo}}
\DeclareOption{leqno}{\input{leqno.clo}}
\DeclareOption{fleqn}{\input{fleqn.clo}}
\DeclareOption{twoside}{\@twosidetrue  \@mparswitchtrue}

\DeclareOption{cours}{%
\newgray{couleurfond}{0.9}
\setboolean{presentation}{false}%
%
\setlength\paperheight{297mm}
\setlength\paperwidth{210mm}
%
\setlength{\textwidth}{165mm}%
\setlength{\oddsidemargin}{4.6mm}%
\setlength{\evensidemargin}{-10.4mm}%
\setlength{\textheight}{260mm}%
\setlength{\headheight}{0mm}%
\setlength{\topmargin}{-15.4mm}%
\setlength{\headsep}{0mm}%
\setlength{\topskip}{0mm}%
\setlength{\footskip}{5mm}%
\setlength{\footeroffset}{10mm}%
%
\setlength{\parindent}{0mm}%
\setlength{\parskip}{3mm}%
\parskip 3mm%
%
\setlength{\largeurtitre}{165mm}%
\setlength{\hauteurtitre}{65mm}%
\setlength{\hauteurlogo}{50mm}%
\setlength{\hauteurdiapotitre}{5mm}%
\setlength{\hauteurdiapoclip}{87mm}%
\setlength{\hauteurdiapototale}{90mm}%
\setlength{\hauteurdiapo}{75mm}%
\setlength{\hauteurimage}{80mm}%
\setlength{\hauteurimagedia}{65mm}%
\DeclareFixedFont{\FonteCode}{\encodingdefault}{pcr}{b}{n}{12pt}%
}% cours

\DeclareOption{presentation}{%
\setboolean{presentation}{true}%
\newgray{couleurfond}{1}
%
\setlength\paperwidth{297mm}%
\setlength\paperheight{210mm}%
%
\setlength{\textwidth}{277mm}%
\setlength{\oddsidemargin}{-15.4mm}%
\setlength{\evensidemargin}{-15.4mm}%
\setlength{\textheight}{170mm}%
\setlength{\headheight}{0mm}%
\setlength{\topmargin}{-15.4mm}%
\setlength{\headsep}{0mm}%
\setlength{\topskip}{0mm}%
\setlength{\footskip}{5mm}%
\setlength{\footeroffset}{0mm}%
%
\setlength{\parindent}{0mm}%
\setlength{\parskip}{3mm}%
\parskip 3mm%
%
\setlength{\largeurtitre}{230mm}%
\setlength{\hauteurtitre}{100mm}%
\setlength{\hauteurlogo}{50mm}%
\setlength{\hauteurdiapotitre}{10mm}%
\setlength{\hauteurdiapoclip}{167mm}%
\setlength{\hauteurdiapototale}{170mm}%
\setlength{\hauteurdiapo}{155mm}%
\setlength{\hauteurimage}{160mm}%
\setlength{\hauteurimagedia}{140mm}%
\DeclareFixedFont{\FonteCode}{\encodingdefault}{pcr}{b}{n}{24pt}%
}%presentation


\ExecuteOptions{fr}
\ProcessOptions

%\input{size1\@ptsize.clo}
\setlength\lineskip{1\p@}
\setlength\normallineskip{1\p@}
\renewcommand\baselinestretch{}
\@lowpenalty   51
\@medpenalty  151
\@highpenalty 301

\newlength{\largeur}
\setlength{\largeur}{\textwidth}
\addtolength{\largeur}{-4mm}

\newlength{\largeurd}
\setlength{\largeurd}{\textwidth}
\addtolength{\largeurd}{-10mm}

\newlength{\hauteur}
\newlength{\titi}

%\usepackage[cam,axes,a4center]{crop}
%\setlength\paperheight{297mm}
%\setlength\paperwidth{210mm}

\newcommand{\modulename}{}
\newcommand{\themename}{}
\newcommand{\formationname}{}
\renewcommand{\pagename}{}
\newcommand{\descriptioncontent}{}
\renewcommand{\headrule}{\relax}

\def\auteurs{Auteurs inconnus}

\AtBeginDocument{\catcode`\:=11} % désactive le :

%\DeclareFontFamily{8r}{pag}{\hyphenchar\font=-1}

\DeclareFixedFont{\FonteTitreModule}{\encodingdefault}{pag}{b}{n}{58pt}
\DeclareFixedFont{\FonteTitreExercice}{\encodingdefault}{pag}{b}{n}{36pt}
\DeclareFixedFont{\FonteObjectifs}{\encodingdefault}{pag}{m}{n}{24pt}
\DeclareFixedFont{\FonteNumPage}{\encodingdefault}{pag}{b}{n}{10pt}
\DeclareFixedFont{\FonteTitre}{\encodingdefault}{pag}{m}{n}{22pt}
\DeclareFixedFont{\FontePage}{\encodingdefault}{pag}{m}{n}{14pt}
\DeclareFixedFont{\FonteDescription}{\encodingdefault}{phv}{m}{n}{11pt}
\DeclareFixedFont{\FonteTexte}{\encodingdefault}{phv}{m}{n}{11pt}
\DeclareFixedFont{\FonteCopy}{\encodingdefault}{phv}{m}{n}{7pt}
\DeclareFixedFont{\FonteEmph}{\encodingdefault}{phv}{m}{sl}{11pt}
\DeclareFixedFont{\FonteMenu}{\encodingdefault}{phv}{b}{n}{11pt}

\renewcommand{\familydefault}{phv}
\renewcommand{\rmdefault}{phv}

\renewcommand{\ttdefault}{pcr}
\renewcommand{\ttfamily}{\fontencoding{\encodingdefault}\fontfamily{pcr}\fontseries{bx}\fontsize{12pt}{15pt}\selectfont}
%\renewcommand{\ttfamily}{\FonteCode\fontseries{bx}\selectfont}
\renewcommand{\sldefault}{sl}
\renewcommand{\itdefault}{sl}
\renewcommand{\updefault}{n}
\renewcommand{\itshape}{\fontencoding{\encodingdefault}\fontshape{sl}\selectfont}
\renewcommand{\upshape}{\fontencoding{\encodingdefault}\fontshape{n}\selectfont}
\renewcommand{\rmfamily}{\fontencoding{\encodingdefault}\fontfamily{phv}\selectfont}


%\DeclareOldFontCommand{\rm}{\fontencoding{\encodingdefault}\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\rm}{\Texte}{\mathrm}
%\DeclareOldFontCommand{\sf}{\fontencoding{\encodingdefault}\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\fontencoding{\encodingdefault}\fontfamily{pcr}\fontseries{bx}\selectfont}{\mathtt}
\DeclareOldFontCommand{\bf}{\FonteMenu}{\mathbf}
\DeclareOldFontCommand{\it}{\FonteEmph}{\mathit}
\DeclareOldFontCommand{\sl}{\FonteEmph}{\mathit}



\floatstyle{plain}
\newfloat{figure}{ht}{lof}

\newcommand{\clearemptydoublepage}{\setcounter{regle}{0}\newpage{\thispagestyle{empty}\cleardoublepage}}

\newcommand{\clearemptypage}{\setcounter{regle}{0}\newpage{\thispagestyle{empty}\newpage}}

\long\def\nettoie#1{\mangeblanc{#1}}

\long\def\mangeblanc#1{%
   \ifcat{#1}{ }\else\ifcat\par{#1}\else\stopmange{#1}\fi\fi
\mangeblanc}

\long\def\stopmange#1#2\mangeblanc {\fi\fi{#1}}

\newcommand{\description}[1]{%
  \pagecolor{white}%
  \thispagestyle{empty}%
  {%
    \parskip 0mm%
    \null\vspace{1.8cm}%

    \psframebox[linecolor=LogideeBlue,fillstyle=solid,fillcolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=3mm]{%
     \vbox to 13mm{%
     \null\vfill%
     \hbox to \textwidth{\null\hfill{\white\FonteTitre{%
         \ifnum \value{theme} > 0 \arabic{theme}.\fi%
         \ifnum \value{module} > 0 \arabic{module} \fi\modulename}\hfill\null}}%
     \vfill}%vbox
     }%psframe

  \vspace{2.5cm}
  \psframebox[linecolor=LogideeBlue,linewidth=1.5pt,cornersize=absolute,linearc=3mm]{%
     \parbox{\textwidth}{%
       \parskip 8mm\color{LogideeBlue}\fontsize{12}{25pt}%
         \usefont{\encodingdefault}{phv}{m}{n}#1\removelastskip\vskip\parskip}%
  }%psframe
  }
  \clearemptydoublepage
}

\newcommand{\titrep}[1]{
  \pagenumbering{arabic}
  \setcounter{theme}{0}
  \renewcommand{\formationname}{#1}
  \renewcommand{\modulename}{#1}
  \pageformation{#1}
  \vspace{2.5cm}
}%formation

\newcommand{\titres}[1]{
  \clearemptypage
  \refstepcounter{theme}
  \addcontentsline{toc}{theme}{\Theme~\thetheme~:~#1}
  \setcounter{slide}{0}
  \renewcommand{\themename}{#1}
  \renewcommand{\modulename}{#1}
  \pagetheme{\thetheme.~#1}
  \vspace{2.5cm}
}%formation

\newcommand{\titret}[1]{
  \color{black}
  \pagecolor{white}
  \clearemptypage
  %\ifthenelse{\isodd{pageno}}{}{\clearemptypage} 
  \refstepcounter{module}
  \addcontentsline{toc}{module}{\themodule~#1}
  \renewcommand{\modulename}{#1}
  \pagetitre{\themodule.~#1}
}%module

\newcommand{\nohyphens}{\hyphenpenalty=10000\exhyphenpenalty=10000\relax}

\newcommand{\pageformation}[1]{
  \thispagestyle{empty}
%% Page d entete principale
  \begin{center}
  {
   %\parskip 0mm
   \color{LogideeBlue}
   \null\vfill
   \pagecolor{white}
	\parbox[c][\hauteurtitre][s]{\largeurtitre}{
          \center
	  \vfil
	  \baselineskip=65pt %plus 100pt %minus 20pt
          \FonteTitreModule \nohyphens #1%
	  \vfil
	  }%
  }
  \vfill
  \includegraphics[height=\hauteurlogo]{logo.eps}
  \vfill
  \vfill
  \Copy
  \end{center}
  \vskip 3mm
}%module

\newcommand{\pagetheme}[1]{
  \thispagestyle{empty}
%% Page d entete secondaire
  {
    \begin{center}
    %\parskip 0mm
    \null\vfill
	\parbox[c][\hauteurtitre][s]{\largeurtitre}{%
          \center%
	  \color{LogideeBlue}
	  %\vfil
	  \baselineskip=65pt plus 100pt minus 5pt%
          \FonteTitreModule \nohyphens%
	  \vskip 53pt plus 80pt minus 50pt
	  #1 
	  \vskip 53pt plus 80pt minus 50pt
	  %\vfil
	  }%
  \vfill
  \includegraphics[height=\hauteurlogo]{logo.eps}
  \vfill
  \vfill
  \end{center}
  }
}%pagetheme

\newcommand{\pagetitre}[1]{
  \thispagestyle{empty}
%% Page d entete tertiaire
  {
    \begin{center}
    %\parskip 0mm
    \color{LogideeBlue}
    \null\vfill
        \parbox[c][\hauteurtitre][s]{\largeurtitre}{%
          \center%
          %\vfil
          \baselineskip=65pt plus 100pt minus 5pt%
          \FonteTitreModule \nohyphens%
          \vskip 53pt plus 80pt minus 50pt
          #1 
          \vskip 53pt plus 80pt minus 50pt
          %\vfil
          }%
  \vfill
  \vfill
  \end{center}
  }
  \color{black}
}%pagetitre


\newcommand{\pagenotice}[1]{
\pagestyle{empty}
\newpage
 {
    %\parskip 0mm
     \null\vskip 1cm
     \Support\\

     \vskip 1cm

     {\bfseries #1}

     \Auteurs\auteurs.

     \vfill

     \Avertissement

     \vfill\vfill

     \Remerciements

     \vskip 1cm

     \Logidoc

     \vskip 2cm

     \Droits

     \vskip 1cm

     \Legal

 }
 %\clearemptypage
}

\newcommand{\noticefin}{
%\clearemptypage
%\begin{center}
%\null\vfill
%Conception et réalisation\\
%Logidée\\
%3, quai Kléber,\\
%Tour Sébastopol,\\
%F-67080 Strasbourg\\
%\vfill
%\end{center}
}


\renewcommand{\glossary}{%
\clearemptypage
  \thispagestyle{empty}
  \addcontentsline{toc}{module}{\Glossary}
  \pagetitre{\Glossary}
  \newpage
  \pagestyle{fancyplain}
  \renewcommand{\pagename}{\Glossary}
%\clearemptypage
}%glossary

\newcommand{\exercices}{%
\clearemptypage
  \thispagestyle{empty}
  \setcounter{exercice}{0}
  \addcontentsline{toc}{module}{\Exercices}
  \pagetitre{\Exercices}
  \renewcommand{\pagename}{\Exercices}
  \clearemptypage
%\clearemptypage
}%exercices

\newcommand{\soluces}{%
\clearemptypage
  \thispagestyle{empty}
  \setcounter{exercice}{0}
  \addcontentsline{toc}{module}{\Solutions}
  \pagetitre{\Solutions}
  \renewcommand{\pagename}{\Solutions}
  \clearemptypage
}%soluces

\newcommand{\sommaire}{%
\pagestyle{empty}
\clearemptypage
  {
  \parskip 0pt
  \goodbreak
  \removelastskip
  \vskip 10pt
  \hskip 2mm{\color{LogideeBlue}\fontsize{36}{36pt}\usefont{\encodingdefault}{phv}{b}{n}\Sommaire}\par
  \nobreak
  \vskip 10pt
  }
  \tableofcontents
%\clearpage
}%tabledesmat

\newenvironment{definition}[1]
               {\vskip\parskip%
               {\color{LogideeBlue}\bfseries #1}\par\begingroup\parindent=1cm\narrower\parindent=0cm}
               {\par\endgroup}

\newcommand\glossarydefinition[2]{
\begin{definition}{#1}%
#2%
\end{definition}}%glossaryterm

\newcommand\glossaryterm[1]{
\psframebox[linecolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=1mm]{#1}%
}

\newcommand{\cmd}[1]{
\FonteCode\color{LogideeBlue}\expandafter{\verb£}#1£
}

\setcounter{secnumdepth}{3}
\newcounter {section}
\newcounter {subsection}[section]
\newcounter {subsubsection}[subsection]
\newcounter {exercice}
\newcounter {theme}
\newcounter {module}[theme]
\newcounter {slide}[module]
\renewcommand \thesection {}
\renewcommand \thesubsection {}
\renewcommand \thesubsubsection {}
%\renewcommand\theexercice{Exercice~\arabic{exercice}~:~}
\renewcommand\theexercice{Exercice~\arabic{exercice}}
\renewcommand\themodule{\arabic{theme}.\arabic{module}}
\renewcommand\theslide{\ifnum \value{theme} > 0 \arabic{theme}.\fi%
                       \ifnum \value{module} > 0 \arabic{module}.\fi%
                       \arabic{slide}}
%\renewcommand \theexercice {}
\newcounter{regle}

\newcommand\page[1]{%
  \setcounter{regle}{0}
  \clearpage
  \pagestyle{fancyplain}
  \refstepcounter{slide}
  \renewcommand{\pagename}{#1}
  %\addcontentsline{toc}{page}{\theslide~#1}
  \addcontentsline{toc}{page}{\theslide~#1}
  \enlargethispage{0.75\baselineskip}
  \def\titrepage{#1}
}

\newcommand\section[1]{%
\removelastskip\vskip 7mm
\goodbreak
\refstepcounter{section}
\addcontentsline{toc}{section}{#1}
\hbox{\fontsize{13}{20pt}\usefont{\encodingdefault}{phv}{b}{n}\color{LogideeBlue}#1}\nobreak
}

\newcommand\subsection[1]{%
\removelastskip\vskip 4mm%
\goodbreak%
\refstepcounter{subsection}%
\addcontentsline{toc}{subsection}{#1}%
\hbox{%
  \fontsize{11}{20pt}%
  \usefont{\encodingdefault}{phv}{b}{n}%
  \color{LogideeBlue}#1%
}\nobreak\nobreak%
}

\newcommand\subsubsection[1]{%
\removelastskip\vskip 4mm%
\goodbreak%
\refstepcounter{subsubsection}%
\addcontentsline{toc}{subsubsection}{#1}%
\hbox{%
  \fontsize{11}{20pt}%
  \usefont{\encodingdefault}{phv}{b}{n}%
  \color{LogideeBlue}#1%
}\nobreak\nobreak%
}

\newcommand{\exercice}[1]{%
  \stepcounter{exercice}%
  \addcontentsline{toc}{exercice}{\theexercice}%
  \removelastskip\vskip 7mm
  {%
    \fontsize{13}{20pt}\usefont{\encodingdefault}{phv}{b}{n}\color{LogideeBlue}\Exercice~\arabic{exercice}%
  }
  \vskip 1mm
  \label{exercice\arabic{exercice}}
}%

\newcommand{\exercicesansreponse}[1]{%
  \stepcounter{exercice}%
}%

\newenvironment{question}[1][]
{\stepcounter{exercice}%
 \def\truc{#1}%
 \setbox77=\vbox\bgroup%
   \hsize=140mm%
   \ifx \truc\empty{}\else%
     \parshape=2 0cm 10cm 0cm 14cm%
   \fi%
   \nettoie}%
{%
  \par%
  \hbox to \hsize{%
    \null\hfill\em \Reponsepage~\pageref{exercice\arabic{exercice}}%
  }%
  \egroup%
  \par%
  \begin{center}%
    \ifx\truc\empty%
      \par%
    \else\vskip 5mm%
    \fi%
    \vbox{%
      \psframebox[linecolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=1.5mm]{%
         \vbox{%
           \hbox{%
             \rule{0pt}{2mm}%
	   }%
           \copy77\par%
         }%
       }%
      \setlength{\hauteur}{\ht77}%
      \addtolength{\hauteur}{3.5mm}%
      \rput(-12cm,\hauteur){%
        \psframebox[boxsep=false,fillcolor=LogideeBlue,fillstyle=solid,linecolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=1.5mm]{%
          \color{white}Exercice~\arabic{exercice}%
        }%
      }%
      \ifx \truc\empty%
        {}%
      \else%
        \cput[linecolor=LogideeBlue,linewidth=1pt,fillcolor=white,fillstyle=solid,framesep=0pt](-2cm,\hauteur){\includegraphics[height=1cm,width=1cm]{\truc}}%
      \fi%
    }%
  \end{center}%
}

\newenvironment{questionsansreponse}[1][]
{\stepcounter{exercice}%
 \def\truc{#1}%
 \setbox77=\vbox\bgroup%
   \hsize=140mm%
   \ifx \truc\empty{}\else%
     \parshape=2 0cm 10cm 0cm 14cm%
   \fi%
   \nettoie}%
{%
  \par%
  \egroup%
  \par%
  \begin{center}%
    \ifx\truc\empty%
      \par%
    \else\vskip 5mm%
    \fi%
    \vbox{%
      \psframebox[linecolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=1.5mm]{%
         \vbox{%
           \hbox{%
             \rule{0pt}{2mm}%
	   }%
           \copy77\par%
         }%
       }%
      \setlength{\hauteur}{\ht77}%
      \addtolength{\hauteur}{3.5mm}%
      \rput(-12cm,\hauteur){%
        \psframebox[boxsep=false,fillcolor=LogideeBlue,fillstyle=solid,linecolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=1.5mm]{%
          \color{white}Exercice~\arabic{exercice}%
        }%
      }%
      \ifx \truc\empty%
        {}%
      \else%
        \cput[linecolor=LogideeBlue,linewidth=1pt,fillcolor=white,fillstyle=solid,framesep=0pt](-2cm,\hauteur){\includegraphics[height=1cm,width=1cm]{\truc}}%
      \fi%
    }%
  \end{center}%
}

\newenvironment{reponse}
	       {\vskip\parskip%
	       {\color{LogideeBlue}\bfseries \Reponse}\par\begingroup\parindent=1cm\narrower\parindent=0cm}
               {\par\endgroup\vskip\parskip}

\newenvironment{objectifs}
	       {\color{white}\list{\ding{111}}{\listparindent 0pt
	       %{\color{LogideeBlue}\list{\ding{111}}{\listparindent 0pt
                \listparindent 0pt
                \itemindent 0pt
                \parskip 0pt
                \partopsep 0pt
                \parsep 0pt
                \itemsep 0pt
                \topsep 0pt
		\labelsep 1cm
		\labelwidth 35pt
		\setlength{\leftmargin}{\labelsep}
		\addtolength{\leftmargin}{\labelwidth}
                \fontsize{24}{30pt}\usefont{\encodingdefault}{pag}{m}{n}
	       }}
	       {
                \endlist\clearemptydoublepage\color{black}
	       }

\newenvironment{note}[1][]
{\def\truc{#1}%
 \setbox77=\vbox\bgroup%
   \hsize=140mm%
   \ifx \truc\empty{}\else%
     \parshape=2 0cm 10cm 0cm 14cm%
   \fi%
   \nettoie}%
{%
  \egroup%
  \par%
  \begin{center}%
    \ifx\truc\empty%
      \par%
    \else\vskip 5mm%
    \fi%
    \vbox{%
      \psframebox[linecolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=1.5mm]{%
         \vbox{%
           \hbox{%
             \rule{0pt}{2mm}%
	   }%
           \copy77\par%
         }%
       }%
      \setlength{\hauteur}{\ht77}%
      \addtolength{\hauteur}{3.5mm}%
      \rput(-12cm,\hauteur){%
        \psframebox[boxsep=false,fillcolor=LogideeBlue,fillstyle=solid,linecolor=LogideeBlue,linewidth=1pt,cornersize=absolute,linearc=1.5mm]{%
          \color{white}Note%
        }%
      }%
      \ifx \truc\empty%
        {}%
      \else%
        \cput[linecolor=LogideeBlue,linewidth=1pt,fillcolor=white,fillstyle=solid,framesep=0pt](-2cm,\hauteur){\includegraphics[height=1cm,width=1cm]{\truc}}%
      \fi%
    }%
  \end{center}
  \par%
}

\newcommand\marge[2]{%
\setbox7=\hbox{$%
 \vcenter{\hsize 24mm%
  \includegraphics[width=2cm]{#1}
 }%
$}\ht7=0pt\dp7=0pt%
\par
\hskip -24mm
\hbox{%
 \box7%
  $\vcenter{#2}$%
 }%
 \removelastskip
}

\newenvironment{code}{%
 \removelastskip\par%
 \nointerlineskip%
 \begin{alltt}%
 \leftskip 5mm%
 \nohyphens%
 \color{LogideeBlue}%
 \FonteCode%
}%
{\end{alltt}\nointerlineskip%
\removelastskip}

\newcommand{\titrediapo}{%
  \setbox77=\hbox{\psframebox[fillcolor=LogideeBlue,fillstyle=solid,linecolor=LogideeBlue,linewidth=1.5pt,cornersize=absolute,linearc=0mm]{%
    \hbox to\textwidth{\null\hfill\color{white}\rule{0pt}{.45cm}\large\theslide~:~\titrepage\hfill}%
    }%psframe
  }%
  \setlength\tmp{\ht77}%
  \addtolength\tmp{-\parskip}%
  \psclip{\psframe[boxsep=false,framesep=0pt,linecolor=LogideeBlue,linewidth=5pt,cornersize=absolute,linearc=3mm](0,-\tmp)(\wd77,\ht77)}\box77\endpsclip%
}

\newenvironment{diapo}[2][]
               {\def\fonddia{#1}%
	        \def\titredia{#2}%
		\ifthenelse{\boolean{presentation}}{\huge}{}%
	        \Sbox%
		\minipage[b][\hauteurdiapo][s]{0.95\largeur}}%
               {\vfill\null%
	        \endminipage%
		\endSbox%
                \psclip{\psframe[boxsep=false,framesep=0pt,linecolor=LogideeBlue,linewidth=5pt,cornersize=absolute,linearc=3mm](0,0)(\textwidth,\hauteurdiapoclip)}%
		\ifx\fonddia\empty%
		\def\diacmd{\psframebox[fillcolor=couleurfond,fillstyle=solid,linecolor=couleurfond,linewidth=1.5pt,cornersize=absolute,linearc=0mm,framesep=0pt,boxsep=false]}%
		\else%
		\def\diacmd{\psframebox[fillcolor=white,fillstyle=solid,linecolor=white,linewidth=1.5pt,framesep=0pt,boxsep=false]}%
		\fi%
		\diacmd{%
		  \vbox to \hauteurdiapototale{%
		    \psframebox[fillcolor=LogideeBlue,fillstyle=solid,linecolor=LogideeBlue,linewidth=1.5pt,cornersize=absolute,linearc=0mm]{%
		      \vbox to \hauteurdiapotitre{\null\vfill\hbox to\textwidth{\null\hfill\ifthenelse{\boolean{presentation}}{\Huge}{} \color{white}\theslide~:~\titredia\hfill}\vfill\null}%
		    }%psframe
		    \ifx \fonddia\empty%
		    \else%
		      \vbox to 0pt{\hbox to 0pt{%
		        \includegraphics[width=\textwidth,height=\hauteurimage]{\fonddia}%
			}}%
		    \fi%
		    \vskip \baselineskip%
		    \hbox to \largeur{\null\hfill\TheSbox\hfill\null}%
		  }%vbox
		}%psframe
		\endpsclip%
		\removelastskip%
		}

\newenvironment{liste}
               {\list{\color{LogideeBlue}\ding{111}}{%
		\listparindent 0pt%
	        \itemindent 0pt%
		\labelsep 5pt%
		\parskip 0pt%
		\partopsep 0pt%
		\parsep 0pt%
		\itemsep 0.65em%
		\labelwidth 0pt%
		\topsep 0.65em%
		\rightmargin 0mm \leftmargin 10mm%
	        }}
               {\endlist\nointerlineskip\par}

\newcommand{\image}[3]{
  \def\legende{#3}%
  \begin{center}
  \psframebox[framesep=0pt,linecolor=LogideeBlue,linewidth=2pt,cornersize=absolute,linearc=3mm]{%
    \vbox{%
      \hbox{\rule{0pt}{1mm}}%
      \ifdim #2pt>0pt%
        \includegraphics[scale=#2]{#1}%
      \else%
        \includegraphics[width=\largeur]{#1}%
      \fi%
      \par%
      \ifx \temp\empty%
        \hbox{\rule{0pt}{1mm}}%
      \else%
        \hbox{\rule{0pt}{5mm}}%
      \fi%
    }%
  }%
  \ifx\legende\empty{}\else%
  \newline
  %\vskip -5mm
  %\par
  \setbox77=\hbox to 10cm{\parbox[b]{10cm}{\color{white}#3}}%
  \rput[t](0,0.7){%
    \psframebox[linecolor=LogideeBlue,linewidth=2pt,cornersize=absolute,linearc=3mm,fillcolor=LogideeBlue,fillstyle=solid]{\copy77}%
  }%
  \setlength{\titi}{\ht77}
  \addtolength{\titi}{-5mm}
  \vskip \titi
  \fi%
  \end{center} 
}

\newcommand{\imagedia}[1]{%
\setbox77=\hbox{\includegraphics[width=\largeurd]{#1}}%
\ifdim \ht77>\hauteurimagedia\setbox77=\hbox{\includegraphics[height=\hauteurimagedia]{#1}}\fi%
\vbox to 7cm{\null\vfill%
  \hbox to\largeur{%
    \null\hfill\box77\hfill\null%
  }%
  \vfill%
 }%
}

\newcommand\tableofcontents{%
    {%
    \parskip 0mm%
    \setcounter{tocdepth}{1}%
    %\baselineskip 25pt%
    %\fontsize{24}{30pt}\usefont{\encodingdefault}{pag}{m}{n}%
    \@starttoc{toc}%
    %\clearemptydoublepage%
    }
}

\def\toto{\leaders \hbox to 2mm{\hfil.\hfil}\hfill}
%\def\toto{\dotfill}

\renewcommand{\numberline}[1]{\hspace{1cm}}

\newcommand*\l@theme[2]{%
  {
  \parskip 0pt
  \goodbreak
  \removelastskip
  \vskip 10pt
  \hbox to 160mm{\hskip 2mm{\color{LogideeBlue}\fontsize{13}{13pt}\usefont{\encodingdefault}{phv}{b}{n}#1}\toto{\color{LogideeBlue}\fontsize{13}{13pt}\usefont{\encodingdefault}{phv}{b}{n}#2}}\par
  \nobreak
  }
}
\newcommand*\l@module[2]{%
  {
  \parskip 0pt
  \goodbreak
  \removelastskip
  \vskip 10pt
  \hbox to 160mm{\hskip 2mm{\color{LogideeBlue}\fontsize{13}{13pt}\usefont{\encodingdefault}{phv}{b}{n}#1}\toto{\color{LogideeBlue}\fontsize{13}{13pt}\usefont{\encodingdefault}{phv}{b}{n}#2}}\par
  }
  \nobreak
  \vskip 5pt
}

\newcommand*\l@page[2]{%
  {\parskip 20pt
  %\nointerlineskip
  \hbox to 160mm{\hskip 2mm{\fontsize{11}{13pt}\usefont{\encodingdefault}{phv}{b}{n}#1}\toto{\fontsize{11}{13pt}\usefont{\encodingdefault}{phv}{b}{n}#2}}\par
}}

\newcommand*\l@section[2]{%
  {\parskip 20pt
  %\nointerlineskip
  \hbox to 160mm{\hskip 7mm{\fontsize{11}{13pt}\usefont{\encodingdefault}{phv}{m}{n}#1}\toto{\fontsize{11}{13pt}\usefont{\encodingdefault}{phv}{m}{n}#2}}\par
}}

\newcommand*\l@subsection[2]{%
}

\newcommand*\l@subsubsection[2]{%
}

\newcommand*\l@exercice[2]{%
%{\fontsize{10}{20pt}\usefont{\encodingdefault}{phv}{m}{n}#1}\toto{\fontsize{10}{20pt}\usefont{\encodingdefault}{phv}{m}{n}#2}\par
}

\sloppy


\fancyhf{}

\fancyfoot[LO,RE]{\rput[bl]{90}(-1,1){\color{black}\FonteCopy \Copy}}

%\fancyfoot[LE,RO]{\rput[tr]{0}(+1.2,+1.2){%
\fancyfoot[LE,RO]{\rput[tr]{0}(\footeroffset,0){%
\psframebox[fillcolor=couleurfond,fillstyle=solid,linecolor=LogideeBlue,linewidth=1.5pt,cornersize=absolute,linearc=3mm]{%
\color{LogideeBlue}\arrayrulewidth 1pt
\begin{tabular}{c|c|c}%
  \begin{tabular}{p{6.5cm}}%
  \parbox{6.5cm}{\color{black}\scriptsize\modulename\hfill}\\
  \hline
  \parbox{6.5cm}{\color{black}\scriptsize\hfill\pagename\hskip 1mm\null}
  \end{tabular} 
& 
\parbox[b]{9mm}{\color{black}\null\hfill\large\thepage\hfill\null} 
& 
\parbox{12mm}{\includegraphics[width=12mm,height=14mm]{logo.eps}}
\end{tabular}}
}}


\pagestyle{fancyplain}
\pagenumbering{arabic}

\setlength\arrayrulewidth{1pt}
\setlength\arraycolsep{5pt}
\setlength\tabcolsep{6pt}
\newcommand{\arraystrech}{1.5}
\raggedbottom

\endinput
%%
%% End of file `article.cls'.
