#!/usr/bin/perl -w

open(FIND, "find . -name '*.xml' |") || die "Can't open pipe: $!\n";
my (@modules, @formations, @themes);
while (defined($_ = <FIND>))
{
    chomp;
    s#^./##;
    next if /^charte/;
    if (m#/formation\S+.xml#)
    {
	push @formations, $_;
    } elsif (m#/theme\S+.xml#)
    {
	push @themes, $_;
    } else 
    {
	push @modules, $_;
    }
}
close(FIND) || die "Problem with the pipe: $!";

print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
print "<!DOCTYPE catalog SYSTEM \"dtd/catalogue.dtd\">\n";
print "<catalog>\n";
print "<modules>\n\n";
foreach (@modules)
{
    print "  <module>\n    <file>";
    print $_;
    print "</file>\n";
    print "    <trainers>\n    </trainers>\n";
    print "    <price value=\"100\"/>\n";
    print "  </module>\n\n";
}
print "</modules>\n";

print "<themes>\n\n";
foreach (@themes)
{
    print "  <theme>\n    <file>";
    print $_;
    print "</file>\n";
    print "    <trainers>\n    </trainers>\n";
    print "    <price value=\"300\"/>\n";
    print "  </theme>\n\n";
}
print "</themes>\n";

print "<formations>\n\n";
foreach (@formations)
{
    print "  <formation>\n    <file>";
    print $_;
    print "</file>\n";
    print "    <trainers>\n    </trainers>\n";
    print "    <price value=\"1000\"/>\n";
    print "  </formation>\n\n";
}
print "</formations>\n";

print "<categories>\n<!--\n";
print "  <category>\n    <title></title>\n";
print "    <include>\n    </include>\n";
print "  </category>\n-->\n";
print "</categories>\n";

print "</catalog>\n";
