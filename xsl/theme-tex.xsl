<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="module-tex.xsl"/> 

<xsl:output method="text" encoding="UTF-8"/>

<xsl:template match="theme">
  <xsl:call-template name="begindoc"/>
  <xsl:apply-templates select="info" mode="theme"/>
  <xsl:apply-templates select="module"/>
  <xsl:call-template name="enddoc"/>
</xsl:template>

<xsl:template match="module">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="info" mode="theme">
  <xsl:text>\titrep{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:text>\pagenotice{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;\sommaire&#10;</xsl:text>
  <xsl:apply-templates select="description"/>
  <xsl:apply-templates select="objectives"/>
</xsl:template>

<xsl:template match="info">
  <xsl:text>\titres{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:apply-templates select="description"/>
  <xsl:apply-templates select="objectives"/>
</xsl:template>

</xsl:stylesheet>

