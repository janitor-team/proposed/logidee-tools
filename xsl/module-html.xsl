<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:param name="selection" select="'all'"/>
<xsl:param name="trainer" select="'false'"/>
<xsl:param name="dir" select="'.'"/>

<xsl:output encoding="UTF-8" 
            omit-xml-declaration="yes" 
	    indent="yes"
	    method="html"/>

<xsl:strip-space elements="title table row col"/>
<xsl:preserve-space elements="code"/>

<xsl:include href="charte.xsl"/>
<xsl:include href="html.xsl"/>


<!-- Pas de diaporama ici ! -->
<xsl:template match="slideshow">
  <xsl:message terminate="yes">
    <xsl:text>ERROR : A slideshow (in file file.xml) must be built with make file_slideshow !</xsl:text>
  </xsl:message>
</xsl:template>

<!-- Le module -->
<xsl:template match="module">

  <html>
  <head>
  <title>
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">module_summary</xsl:with-param>
  </xsl:call-template>
  <xsl:text> « </xsl:text>
  <xsl:value-of select="info/title"/>
  <xsl:text> »</xsl:text>
  </title>
  <link rel="stylesheet" href="default.css" type="text/css"/>
  </head>
  
  <body>

  <table width="100%">
  <tr><td align="left">
  <div class="barnavtop">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">index</xsl:with-param>
  </xsl:call-template>
  </div>
  </td><td align="right" class="barnav">
  <xsl:text> [ </xsl:text>
  <a class="barnav" target="_top" href="frame.html"><xsl:call-template
  name="gettext">
    <xsl:with-param name="id">with_frames</xsl:with-param>
  </xsl:call-template></a>
  <xsl:text> | </xsl:text>
  <a class="barnav" target="_top" href="index.html"><xsl:call-template
  name="gettext">
    <xsl:with-param name="id">without_frames</xsl:with-param>
  </xsl:call-template></a>
  <xsl:text> ]</xsl:text>
  </td></tr>
  </table>

  <h1 class="sommaire"><xsl:value-of select="info/title"/></h1>
  
  <h2 class="sommaire"><a name="des">
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Description</xsl:with-param>
  </xsl:call-template>
  </a>
  </h2>
  <div class="description">
  <xsl:apply-templates select="info/description"/>
  <p class="para">
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Version</xsl:with-param>
  </xsl:call-template>
  <xsl:text> : </xsl:text>
  <!-- TODO: faut savoir laquelle est la dernière version -->
  <xsl:value-of select="info/version[position()=last()]/@number"/>
  <xsl:if test="info/version[position()=last()]/date">
  <br/>
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Date</xsl:with-param>
  </xsl:call-template>
  <xsl:text> : </xsl:text>
  <xsl:value-of select="info/version[position()=last()]/date"/>
  </xsl:if>
  </p>
  </div>

  <h2 class="sommaire"><a name="auteurs">
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Authors</xsl:with-param>
  </xsl:call-template>
  </a></h2>
  <div class="auteurs">
  <ul>
  <xsl:for-each select="//author">
    <xsl:variable name="nom" select="text()"/>
    <xsl:if test="not(preceding::author[text()=$nom])">
      <li><xsl:value-of select="."/>
      <xsl:if test="../email">
        <xsl:text> &lt;</xsl:text>
        <xsl:apply-templates select="../email"/>
        <xsl:text>&gt;</xsl:text>
      </xsl:if>
      </li>
    </xsl:if>
  </xsl:for-each>
  </ul>
  </div>

  <h2 class="sommaire"><a name="sommaire">
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Summary</xsl:with-param>
  </xsl:call-template>
  </a></h2>
  <xsl:call-template name="sommaire"/>
  <div class="barnavbottom">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">index</xsl:with-param>
  </xsl:call-template>
  </div>
  <xsl:call-template name="noticehtml"/>
  </body>
  </html>

  <xsl:apply-templates select="page[@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all']"/>

  <!-- The page of exercises -->
  <xsl:document href="{$dir}/exercises.html"
		method="html"
		encoding="UTF-8"
		omit-xml-declaration="yes">
  <html>
  <head>
  <title>
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">module_exercises</xsl:with-param>
  </xsl:call-template>
  <xsl:text> « </xsl:text>
  <xsl:value-of select="info/title"/>
  <xsl:text> »</xsl:text>
  </title>
  <link rel="stylesheet" href="default.css" type="text/css"/>
  </head>
  <body>
  <div class="barnavtop">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">exo</xsl:with-param>
  </xsl:call-template>
  </div>
    <h1>
    <xsl:call-template name="gettext">
      <xsl:with-param name="id">module_exercises</xsl:with-param>
    </xsl:call-template>
    <xsl:text> « </xsl:text>
    <xsl:value-of select="info/title"/>
    <xsl:text> »</xsl:text>
    </h1>
    <xsl:apply-templates select="//exercise" mode="exercice"/>
  <div class="barnavbottom">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">exo</xsl:with-param>
  </xsl:call-template>
  </div>
  <xsl:call-template name="noticehtml"/>
  </body>
  </html>
  </xsl:document>
 
  <!-- The page of glossary -->
  <xsl:document href="{$dir}/glossary.html"
		method="html"
		encoding="UTF-8"
		omit-xml-declaration="yes">
  <html>
  <head>
  <title>
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Glossary</xsl:with-param>
  </xsl:call-template>
  </title>
  <link rel="stylesheet" href="default.css" type="text/css"/>
  </head>
  <body>
  <div class="barnavtop">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">glossary</xsl:with-param>
  </xsl:call-template>
  </div>
    <h1>
    <xsl:call-template name="gettext">
      <xsl:with-param name="id">Glossary</xsl:with-param>
    </xsl:call-template>
    </h1>
    <dl>
    <xsl:apply-templates select="//glossary[text() != '']" mode="glossary">
      <xsl:sort select="@name"/>
    </xsl:apply-templates>
    </dl>
  <div class="barnavbottom">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">glossary</xsl:with-param>
  </xsl:call-template>
  </div>
  <xsl:call-template name="noticehtml"/>
  </body>
  </html>
  </xsl:document>

  <!-- The summary only -->
  <xsl:document href="{$dir}/summary.html"
		output="html" encoding="UTF-8"
		omit-xml-declaration="yes">
  <html>
  <head>
    <title>
    <xsl:call-template name="gettext">
      <xsl:with-param name="id">module_summary</xsl:with-param>
    </xsl:call-template>
    <xsl:text> « </xsl:text>
    <xsl:value-of select="info/title"/>
    <xsl:text> »</xsl:text>
    </title>
    <link rel="stylesheet" href="default.css" type="text/css"/>
  </head>
  <body>
  <h1>
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Summary</xsl:with-param>
  </xsl:call-template>
  </h1>
  <xsl:call-template name="sommaire">
    <xsl:with-param name="target">cours</xsl:with-param>
  </xsl:call-template>
  </body>
  </html>
  </xsl:document>

  <!-- The frame -->
  <xsl:document href="{$dir}/frame.html"
		output="html" encoding="UTF-8"
		omit-xml-declaration="yes">
  <html>
  <head>
    <title><xsl:value-of select="info/title"/></title>
    <link rel="stylesheet" href="default.css" type="text/css"/>
  </head>
  <frameset cols="25%,*">
    <frame src="summary.html" name="sommaire"/>
    <frame src="index.html" name="cours"/>
    <noframes>
    <body>
    <a href="index.html"><xsl:call-template name="gettext">
      <xsl:with-param name="id">click_here</xsl:with-param>
    </xsl:call-template></a>.
    </body>
    </noframes>
  </frameset>
  </html>
  </xsl:document>
 
  <!-- List of images -->
  <xsl:document href="{$dir}/images"
		output="text" encoding="UTF-8"
		omit-xml-declaration="yes">
    <xsl:text>&#10;</xsl:text><!-- file not empty -->
    <xsl:for-each select="//image">
      <xsl:value-of select="@src"/>
      <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
    <xsl:for-each select="//@icon[contains(string(.), '.eps')]">
      <xsl:value-of select="."/>
      <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
  </xsl:document>
  
</xsl:template>

<!-- Summary -->
<xsl:template name="sommaire">
  <xsl:param name="target" select="''"/>

  <ol>
  <xsl:for-each select="page[@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all']">
    <li>
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:text>page</xsl:text>
        <xsl:number level="any" count="page" format="1" />
        <xsl:text>.html</xsl:text>
      </xsl:attribute>
      <xsl:if test="string($target)!=''">
        <xsl:attribute name="target">
	  <xsl:value-of select="string($target)"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:attribute name="class">titresommaire</xsl:attribute>
      <xsl:apply-templates select="title" mode="sommaire"/>
    </xsl:element>
    <ul>
    <xsl:for-each select="section[@restriction='all' or @resriction=$selection 
     or not(@restriction) or $selection='all']">
      <li>
      <xsl:element name="a">
        <xsl:attribute name="href">
          <xsl:text>page</xsl:text>
          <xsl:number level="any" count="page" format="1" />
          <xsl:text>.html#section</xsl:text>
          <xsl:number level="single" count="section" format="1" />
	</xsl:attribute>
	<xsl:if test="string($target)!=''">
          <xsl:attribute name="target">
	    <xsl:value-of select="string($target)"/>
	  </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="class">titresommaire2</xsl:attribute>
        <xsl:apply-templates select="title" mode="sommaire"/>
      </xsl:element>
      </li>
    </xsl:for-each>
    </ul>
    </li>
  </xsl:for-each>
  <li>
  <xsl:element name="a">
    <xsl:attribute name="href">
      <xsl:text>exercises.html</xsl:text>
    </xsl:attribute>
    <xsl:attribute name="class">titresommaire</xsl:attribute>
    <xsl:if test="string($target)!=''">
      <xsl:attribute name="target">
        <xsl:value-of select="string($target)"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:call-template name="gettext">
      <xsl:with-param name="id">Exercises</xsl:with-param>
    </xsl:call-template>
  </xsl:element>
  </li>
  <li>
  <xsl:element name="a">
    <xsl:attribute name="href">
      <xsl:text>glossary.html</xsl:text>
    </xsl:attribute>
    <xsl:attribute name="class">titresommaire</xsl:attribute>
    <xsl:if test="string($target)!=''">
      <xsl:attribute name="target">
        <xsl:value-of select="string($target)"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:call-template name="gettext">
      <xsl:with-param name="id">Glossary</xsl:with-param>
    </xsl:call-template>
  </xsl:element>
  </li>

  </ol>
</xsl:template>

<!-- Navigation bar -->
<xsl:template name="barnav">
  <xsl:param name="type" select="'page'"/>
  <xsl:choose>
    <!-- Navigation bar for the exercises page -->
    <xsl:when test="$type='exo'">
      <xsl:text>[ </xsl:text>
      <xsl:element name="a">
        <xsl:attribute name="href">
	  <xsl:text>page</xsl:text>
	  <xsl:value-of select="count(//page)"/>
	  <xsl:text>.html</xsl:text>
	</xsl:attribute>
	<xsl:attribute name="class">barnav</xsl:attribute>
	<xsl:call-template name="gettext">
	  <xsl:with-param name="id">previous_page</xsl:with-param>
	</xsl:call-template>
      </xsl:element>
      <xsl:text> | </xsl:text>
      <a href="index.html#sommaire" class="barnav"><xsl:call-template
      name="gettext">
        <xsl:with-param name="id">Summary</xsl:with-param>
      </xsl:call-template></a>
      <xsl:text> | </xsl:text>
      <xsl:element name="a">
        <xsl:attribute name="href">
	  <xsl:text>glossary.html</xsl:text>
	</xsl:attribute>
	<xsl:call-template name="gettext">
	  <xsl:with-param name="id">next_page</xsl:with-param>
	</xsl:call-template>
      </xsl:element>
      <xsl:text> ]</xsl:text>
    </xsl:when>
    <!-- Navigation bar for the glossary page -->
    <xsl:when test="$type='glossary'">
      <xsl:text>[ </xsl:text>
      <xsl:element name="a">
        <xsl:attribute name="href">
	  <xsl:text>exercises.html</xsl:text>
	</xsl:attribute>
	<xsl:attribute name="class">barnav</xsl:attribute>
	<xsl:call-template name="gettext">
	  <xsl:with-param name="id">previous_page</xsl:with-param>
	</xsl:call-template>
      </xsl:element>
      <xsl:text> | </xsl:text>
      <a href="index.html#sommaire" class="barnav"><xsl:call-template
      name="gettext">
        <xsl:with-param name="id">Summary</xsl:with-param>
      </xsl:call-template></a>
      <xsl:text> | </xsl:text>
      <xsl:call-template name="gettext">
        <xsl:with-param name="id">next_page</xsl:with-param>
      </xsl:call-template>
      <xsl:text> ]</xsl:text>
    </xsl:when>
    <!-- Navigation bar for the summary page -->
    <xsl:when test="$type='index'">
      <xsl:text>[ </xsl:text>
      <xsl:call-template name="gettext">
        <xsl:with-param name="id">previous_page</xsl:with-param>
      </xsl:call-template>
      <xsl:text> | </xsl:text>
      <a href="index.html#sommaire" class="barnav"><xsl:call-template
      name="gettext">
        <xsl:with-param name="id">Summary</xsl:with-param>
      </xsl:call-template></a>
      <xsl:text> | </xsl:text>
      <a href="page1.html" class="barnav"><xsl:call-template name="gettext">
        <xsl:with-param name="id">next_page</xsl:with-param>
      </xsl:call-template></a>
      <xsl:text> ]</xsl:text>
    </xsl:when>
    <!-- Navigation bar for normal pages -->
    <xsl:otherwise>
      <xsl:text>[ </xsl:text>
      <!-- First link -->
      <xsl:choose>
        <!-- Not the first page -->
        <xsl:when test="count(preceding::page)">
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>page</xsl:text>
	      <xsl:value-of select="count(preceding::page)"/>
	      <xsl:text>.html</xsl:text>
	    </xsl:attribute>
	    <xsl:attribute name="class">barnav</xsl:attribute>
	    <xsl:call-template name="gettext">
	      <xsl:with-param name="id">previous_page</xsl:with-param>
	    </xsl:call-template>
	  </xsl:element>
	</xsl:when>
	<!-- First page -->
	<xsl:otherwise>
	  <a href="index.html" class="barnav"><xsl:call-template
	  name="gettext">
	    <xsl:with-param name="id">previous_page</xsl:with-param>
	  </xsl:call-template></a>
	</xsl:otherwise>
      </xsl:choose>
      <!-- Second link -->
      <xsl:text> | </xsl:text>
      <a href="index.html#sommaire" class="barnav"><xsl:call-template
      name="gettext">
        <xsl:with-param name="id">Summary</xsl:with-param>
      </xsl:call-template></a>
      <xsl:text> | </xsl:text>
      <!-- Third link -->
      <xsl:choose>
        <!-- Not the last page -->
        <xsl:when test="count(following::page)">
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>page</xsl:text>
	      <xsl:value-of select="count(preceding::page)+2"/>
	      <xsl:text>.html</xsl:text>
	    </xsl:attribute>
	    <xsl:attribute name="class">barnav</xsl:attribute>
	    <xsl:call-template name="gettext">
	      <xsl:with-param name="id">next_page</xsl:with-param>
	    </xsl:call-template>
	  </xsl:element>
	</xsl:when>
	<!-- Last page -->
	<xsl:otherwise>
	  <a href="exercises.html" class="barnav"><xsl:call-template
	  name="gettext">
	    <xsl:with-param name="id">next_page</xsl:with-param>
	  </xsl:call-template></a>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:text> ]</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- An html file per page -->
<xsl:template match="page">
<xsl:variable name="num" select="count(preceding::page)+1"/>
<xsl:document href="{concat($dir, '/page', $num, '.html')}"
	      method="html"
	      encoding="UTF-8"
	      omit-xml-declaration="yes">
  <html>
  <head>
  <title><xsl:value-of select="title"/></title>
  <link rel="stylesheet" href="default.css" type="text/css"/>
  </head>
  <body>
  <div class="barnavtop">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">page</xsl:with-param>
  </xsl:call-template>
  </div>
  <xsl:apply-templates/>
  <div class="barnavbottom">
  <xsl:call-template name="barnav">
    <xsl:with-param name="type">page</xsl:with-param>
  </xsl:call-template>
  </div>
  <xsl:call-template name="noticehtml"/>
  </body>
  </html>
</xsl:document>
</xsl:template>

<!-- Exercises on their own page -->
<!--<xsl:template match="exercise" mode="exercice">
  <xsl:if test="@icon">
    <xsl:element name="img">
      <xsl:attribute name="src">
        <xsl:call-template name="icone">
	  <xsl:with-param name="output">html</xsl:with-param>
	</xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="class">icone</xsl:attribute>
    </xsl:element>
  </xsl:if>
  <xsl:element name="a">
    <xsl:attribute name="name">
      <xsl:text>exo</xsl:text>
  a<xsl:value-of select="count(exercice)"/>
  b<xsl:value-of select="count(preceding::exercice)"/>
  c<xsl:value-of select="count(//exercice)"/>
  d<xsl:value-of select="count(preceding-sibling::exercice)"/>
    </xsl:attribute>
    <xsl:element name="h2">
      <xsl:call-template name="gettext">
        <xsl:with-param name="id">Exercise</xsl:with-param>
      </xsl:call-template>
      <xsl:text> </xsl:text>
  b<xsl:value-of select="count(preceding::exercice)"/>
    </xsl:element>
  </xsl:element>
  <xsl:choose>
    <xsl:when test="question/para">
      <xsl:apply-templates select="question/para"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="question"/>
    </xsl:otherwise>
  </xsl:choose>
  <div class="reponse">
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">answer</xsl:with-param>
  </xsl:call-template>
  </div>
  <xsl:choose>
    <xsl:when test="answer/para">
      <xsl:apply-templates select="answer/para"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="answer"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
-->

<xsl:template match="exercise" mode="exercice">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:if test="answer">
    <xsl:if test="@icon">
      <xsl:element name="img">
	<xsl:attribute name="src">
	  <xsl:call-template name="icone">
	    <xsl:with-param name="output">html</xsl:with-param>
	  </xsl:call-template>
	</xsl:attribute>
	<xsl:attribute name="class">icone</xsl:attribute>
      </xsl:element>
    </xsl:if>
    <div class="question">
    <table class="question">
    <tr class="question"><th class="cadre">
      <xsl:element name="a">
	<xsl:attribute name="name">
	  <xsl:text>exo</xsl:text>
	  <xsl:number level="any" count="exercise" format="1"/>
	</xsl:attribute>
      </xsl:element>
    <xsl:call-template name="gettext">
      <xsl:with-param name="id">Exercise</xsl:with-param>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:number level="any" count="exercise" format="1"/>
    </th></tr>
    <tr class="question"><td class="question">
    <xsl:if test="@icon">
      <xsl:element name="img">
	<xsl:attribute name="src">
	  <xsl:call-template name="icone">
	    <xsl:with-param name="output">html</xsl:with-param>
	  </xsl:call-template>
	</xsl:attribute>
	<xsl:attribute name="class">icone</xsl:attribute>
      </xsl:element>
    </xsl:if>
    <xsl:apply-templates select="question" mode="question"/>
    </td></tr>
    <tr><td>
    <div class="reponse">
    <xsl:call-template name="gettext">
      <xsl:with-param name="id">answer</xsl:with-param>
    </xsl:call-template>
    </div>
    <xsl:choose>
      <xsl:when test="answer/para">
	<xsl:apply-templates select="answer/para"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates select="answer"/>
      </xsl:otherwise>
    </xsl:choose>
    </td></tr>
    </table>
    </div>
  </xsl:if>
  </xsl:if>
</xsl:template>


<!-- No slide -->
<xsl:template match="slide">
</xsl:template>

<!-- A title within a summary has nothing special -->
<xsl:template match="title" mode="sommaire">
  <xsl:apply-templates/>
</xsl:template>

<!-- Glossary -->
<xsl:template match="glossary">
  <xsl:element name="a">
    <xsl:attribute name="href">
      <xsl:text>glossary.html#</xsl:text>
      <xsl:value-of select="@name"/>
    </xsl:attribute>
    <xsl:value-of select="@name"/>
  </xsl:element>
</xsl:template>

<xsl:template match="glossary" mode="glossary">
  <dt>
    <xsl:element name="a">
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:value-of select="@name"/>
    </xsl:element>
  </dt>
  <dd><xsl:apply-templates/></dd>
</xsl:template>

</xsl:stylesheet>
