<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:param name="selection" select="'all'"/>
<xsl:param name="trainer" select="'false'"/>

<xsl:output method="text" encoding="UTF-8"/>

<xsl:strip-space elements="*"/>
<xsl:preserve-space elements="code"/>


<!-- indent allows to repeat <nb> times the string <car>
     It's used to indent the lists -->
<xsl:template name="indent">
 <xsl:param name="nb"/>
 <xsl:param name="car"/>
 <xsl:if test="$nb &gt; 0">
  <xsl:choose>
   <xsl:when test="$car">
    <xsl:value-of select="$car"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:text> </xsl:text>
   </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="indent">
   <xsl:with-param name="nb" select="$nb - 1"/>
   <xsl:with-param name="car" select="$car"/>
  </xsl:call-template>
 </xsl:if>
</xsl:template>


<!-- No template for <module> -->

<!-- The page -->
<xsl:template match="page">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:apply-templates select="title"/>
  <xsl:if test="not(slide)">
   <xsl:call-template name="diapoauto"/>
  </xsl:if>
  <xsl:apply-templates select="slide|section|exercise"/>
  </xsl:if>
</xsl:template>

<!-- The <info> header -->
<xsl:template match="info">
<xsl:text>

</xsl:text>
<xsl:value-of select="normalize-space(title)"/>
<xsl:text>

DESCRIPTION : </xsl:text>
<xsl:value-of select="normalize-space(description/para)"/>
<xsl:text>

OBJECTIFS :
</xsl:text>
<xsl:for-each select="objectives/item">
<xsl:text>- </xsl:text><xsl:value-of select="normalize-space(.)"/>
</xsl:for-each>
<xsl:text>

RATIO ET DUREE : </xsl:text>
<xsl:value-of select="ratio/@value"/>
<xsl:text> - </xsl:text>
<xsl:value-of select="duration/@value"/>
<xsl:value-of select="duration/@unit"/>
<xsl:text>

VERSION : </xsl:text>
<xsl:value-of select="version[position()=last()]/@number"/>
<xsl:text> par </xsl:text>
<xsl:value-of select="version[position()=last()]/author"/>
<xsl:text>

</xsl:text>
</xsl:template>

<!-- A slide -->
<xsl:template match="slide">
<xsl:text>
--- DIAPO ---</xsl:text>

  <xsl:apply-templates select="title" mode="diapo"/>
  <xsl:choose>
    <xsl:when test="image">
      <xsl:apply-templates select="image"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="list"/>
    </xsl:otherwise>
  </xsl:choose>

<xsl:text>
--- FIN DIAPO ---
</xsl:text>
</xsl:template>

<!-- A generated slide -->
<xsl:template name="diapoauto">
<xsl:text>
--- DIAPO AUTO ---</xsl:text>
  <xsl:apply-templates select="descendant::title" mode="diapo"/>
<xsl:text>
--- FIN DIAPO AUTO ---
</xsl:text>
</xsl:template>

<!-- Titles for generated slides -->
<xsl:template match="title" mode="diapo">
<xsl:text>
</xsl:text>
 <xsl:variable name="nb" select="count(ancestor::section)"/>
 <xsl:call-template name="indent">
  <xsl:with-param name="nb" select="$nb"/>
  <xsl:with-param name="car" select="'  '"/>
 </xsl:call-template>
 <xsl:text>o </xsl:text>
 <xsl:apply-templates/>
</xsl:template>

<!-- Sections, just for verification -->
<xsl:template match="section">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:variable name="level" select="count(ancestor::section)"/>
  <xsl:choose>
    <xsl:when test='$level=0'>
    <xsl:text>

</xsl:text>
    </xsl:when>
    <xsl:when test='$level=1'>
<xsl:text>
</xsl:text>
    </xsl:when>
    <xsl:when test='$level=2'>
<xsl:text>
</xsl:text>
    </xsl:when>
    <xsl:otherwise>
    <xsl:message terminate="yes">
      <xsl:text>ERROR : Sub-sub-sub-sections aren't allowed !</xsl:text>
    </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates/>
  </xsl:if>
</xsl:template>

<!-- Titles -->
<xsl:template match="title">
<xsl:variable name="nb" select="3 - count(ancestor::section)"/>
<xsl:call-template name="indent">
 <xsl:with-param name="nb" select="$nb"/>
 <xsl:with-param name="car" select="'#'"/>
</xsl:call-template>
<xsl:text>
</xsl:text>
<xsl:call-template name="indent">
 <xsl:with-param name="nb" select="$nb"/>
 <xsl:with-param name="car" select="'#'"/>
</xsl:call-template>
<xsl:text> </xsl:text>
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
<xsl:call-template name="indent">
 <xsl:with-param name="nb" select="$nb"/>
 <xsl:with-param name="car" select="'#'"/>
</xsl:call-template>
</xsl:template>

<!-- A paragraph -->
<xsl:template match="para">
<xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
<xsl:text>
</xsl:text>
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
</xsl:if>
</xsl:template>

<!-- Emphasize -->
<xsl:template match="em">
 <xsl:text>*</xsl:text>
 <xsl:apply-templates/>
 <xsl:text>*</xsl:text>
</xsl:template>

<!-- Command/file/menu -->
<xsl:template match="cmd|fichier|menu">
  <xsl:text>_</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>_</xsl:text>
</xsl:template>

<!-- URLs -->
<xsl:template match="url">
 <xsl:choose>
  <xsl:when test="string(.)=''">
   <xsl:value-of select="@href"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="."/>
   <xsl:text> (</xsl:text>
   <xsl:value-of select="@href"/>
   <xsl:text>)</xsl:text>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<!-- Notes -->
<xsl:template match="note">
<xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
<xsl:if test="@trainer=$trainer or $trainer='true' or not(@trainer)">
<xsl:text>
--  NOTE  --
</xsl:text>
    <xsl:apply-templates/>
<xsl:text>
--  FIN NOTE  --
</xsl:text>
</xsl:if>
</xsl:if>
</xsl:template>

<!-- Code listing -->
<xsl:template match="code">
<xsl:text>

</xsl:text>
  <xsl:value-of select="text()"/>
<xsl:text>
</xsl:text>
</xsl:template>

<!-- Lists -->
<xsl:template match="list">
<xsl:text>
</xsl:text>
  <xsl:apply-templates/>
<xsl:text>
</xsl:text>
</xsl:template>

<!-- Items of list -->
<xsl:template match="item">
<xsl:variable name="nb" select="count(ancestor::liste)-1"/>
<xsl:call-template name="indent">
 <xsl:with-param name="nb" select="$nb"/>
 <xsl:with-param name="car" select="'  '"/>
</xsl:call-template>
<xsl:text>o </xsl:text>
<xsl:apply-templates/><xsl:text>
</xsl:text>
</xsl:template>

<!-- Images -->
<xsl:template match="img">
<xsl:text>[ </xsl:text>
<xsl:value-of select="@src"/>
<xsl:text> ]</xsl:text>
</xsl:template>

<!-- Tables -->
<xsl:template match="table">
<xsl:text>

</xsl:text>
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
</xsl:template>

<!-- Row of a table -->
<xsl:template match="row">
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
</xsl:template>

<!-- Cell of a table -->
<xsl:template match="col">
<xsl:apply-templates/>
<xsl:text>	</xsl:text>
</xsl:template>

<!-- Exercise -->
<xsl:template match="exercise">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
    <xsl:apply-templates/>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
