<?xml version="1.0" encoding="UTF-8" ?>
<!--
# $Id$
# Raphaël Hertzog, 2001, tous droits réservés
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:include href="html.xsl"/>

<xsl:param name="dir"/>
<xsl:param name="odir"/>

<xsl:output encoding="UTF-8" method="html"/>

<xsl:variable name="fichiers" select="//file"/>

<xsl:template match="catalogue|catalog">

  <xsl:apply-templates select="modules|formations|themes"/>
  <!-- <xsl:apply-templates/> -->

<!--
  <xsl:document href="{$odir}/formations.html"
		method="html" encoding="UTF-8" indent="yes"
		omit-xml-declaration="yes">
		
  </xsl:document>
  <xsl:document href="{$odir}/themes.html"
		method="html" encoding="UTF-8" indent="yes"
		omit-xml-declaration="yes">
		
  </xsl:document>
-->

  <html>
  <head>
    <title>Catalogue de modules</title>
    <link rel="stylesheet" href="default.css" type="text/css"/>
  </head>
  <body>
  <h1>Catalogue de modules</h1>
  <table border="1" width="90%" align="center">
  <xsl:for-each select="categories/category">
    <xsl:variable name="num" select="count(preceding::category)+1"/>
    <xsl:if test="$num mod 2 != 0">
      <tr>
      <th class="catalogue">
      <xsl:value-of select="//categories/category[$num]/title"/>
      </th>
      <th class="catalogue">
      <xsl:value-of select="//categories/category[$num+1]/title"/>
      </th>
      </tr>
      <tr>
      <td>
      <ul>
      <xsl:for-each select="//categories/category[$num]/include/ref">
	<li>
	<xsl:call-template name="lien">
	  <xsl:with-param name="ref" select="string(.)"/>
	  <xsl:with-param name="title" select="string(@title)"/>
	</xsl:call-template>
	</li>
      </xsl:for-each>
      </ul>
      </td><td>
      <ul>
      <xsl:for-each select="//categories/category[$num+1]/include/ref">
	<li>
	<xsl:call-template name="lien">
	  <xsl:with-param name="ref" select="string(.)"/>
	  <xsl:with-param name="title" select="string(@title)"/>
	</xsl:call-template>
	</li>
      </xsl:for-each>
      </ul>
      </td>
      </tr>
    </xsl:if>
  </xsl:for-each>
  </table>
  </body>
  </html>

</xsl:template>


<!-- Module, theme et formation -->

<xsl:template match="modules|themes|formations">
  <xsl:apply-templates/>  
</xsl:template>

<xsl:template match="module|theme|formation">

  <xsl:variable name="doc" 
		select="document(concat($dir, '/', string(file)))"/>
  <xsl:variable name="ref" select="$doc/*/info/ref"/>
  <xsl:document href="{$odir}/{$ref}.html"
		method="html" encoding="UTF-8" indent="yes"
		omit-xml-declaration="yes">
    <html>
    <head>
      <title><xsl:value-of select="$doc/*/info/title"/></title>
      <link rel="stylesheet" href="default.css" type="text/css"/>
    </head>
    <body>
      <h1><xsl:value-of select="$doc/*/info/title"/></h1>
      <xsl:if test="$doc/*/info/objectives/item[string-length(text())>0]">
        <h2>Objectifs</h2>
        <xsl:apply-templates select="$doc/*/info/objectives"/>
      </xsl:if>
      <h2>Description</h2>
      <xsl:apply-templates select="$doc/*/info/description"/>
      <h2>Caractéristiques</h2>
      <table border="1" width="350">
      <tr><th>Durée</th><th>Théorie/Pratique</th><th>Niveau</th></tr>
      <tr align="center">
      <td><xsl:value-of select="concat($doc/*/info/duration/@value,
				       $doc/*/info/duration/@unit)"/></td>
      <td>
      <xsl:value-of select="substring-before($doc/*/info/ratio/@value, '/')"/>
      <xsl:text>% - </xsl:text>
      <xsl:value-of select="substring-after($doc/*/info/ratio/@value, '/')"/>
      <xsl:text>%</xsl:text>
      </td>
      <td><xsl:value-of select="$doc/*/info/level/@value"/></td>
      </tr>
      </table>
      <xsl:if test="$doc/*/info/prerequisite/para">
	<h2>Prérequis</h2>
        <xsl:apply-templates select="$doc/*/info/prerequisite"/>
      </xsl:if>
      <xsl:if test="$doc/*/info/dependency/ref">
        <h2>Dépendances</h2>
	<ul>
	<xsl:for-each select="$doc/*/info/dependency/ref">
	  <li>
	  <xsl:call-template name="lien">
	    <xsl:with-param name="ref" select="string(.)"/>
	    <xsl:with-param name="long" select="1"/>
	    <xsl:with-param name="java" select="0"/>
	  </xsl:call-template>
	  </li>
	</xsl:for-each>
	</ul>
      </xsl:if>
      <xsl:if test="$doc/*/info/suggestion/ref">
        <h2>Suggestions</h2>
	<ul>
	<xsl:for-each select="$doc/*/info/suggestion/ref">
	  <li>
	  <xsl:call-template name="lien">
	    <xsl:with-param name="ref" select="string(.)"/>
	    <xsl:with-param name="long" select="1"/>
	    <xsl:with-param name="java" select="0"/>
	  </xsl:call-template>
	  </li>
	</xsl:for-each>
	</ul>
      </xsl:if>
      <xsl:if test="$doc/*/*/info/ref">
        <h2>Composition</h2>
	<ul>
	<xsl:for-each select="$doc/*/*/info/ref">
	  <li>
	  <xsl:call-template name="lien">
	    <xsl:with-param name="ref" select="string(.)"/>
	    <xsl:with-param name="long" select="1"/>
	    <xsl:with-param name="java" select="0"/>
	  </xsl:call-template>
	  </li>
	</xsl:for-each>
	</ul>
      </xsl:if>
    </body>
    </html>
  </xsl:document>
  
</xsl:template>

<!-- Affiche un lien vers <ref> -->
<xsl:template name="lien">
  <xsl:param name="ref"/>
  <xsl:param name="title"/>
  <xsl:param name="long"/>
  <xsl:param name="java" select="1"/>

  <xsl:variable name="docfile">
    <xsl:call-template name="getdocfile">
      <xsl:with-param name="ref" select="$ref"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="doc" select="document(string($docfile))"/>
  
  <xsl:element name="a">
    <xsl:attribute name="href">
      <xsl:value-of select="concat($ref, '.html')"/>
    </xsl:attribute>
    <xsl:if test="$java=1">
      <xsl:attribute name="onClick">
        <xsl:text>window.open('</xsl:text>
        <xsl:value-of select="concat($ref, '.html')"/>
	<xsl:text>','module','width=440,height=480,scrollbars=yes'); return false;</xsl:text>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="$long=1">
      <xsl:if test="$doc/module">
	<xsl:text>Module « </xsl:text>
      </xsl:if>
      <xsl:if test="$doc/theme">
	<xsl:text>Theme « </xsl:text>
      </xsl:if>
      <xsl:if test="$doc/formation">
	<xsl:text>Formation « </xsl:text>
      </xsl:if>
    </xsl:if>
    <xsl:choose>
      <xsl:when test="string-length($title)">
        <xsl:value-of select="$title"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="$doc/*/info/title"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="$long=1">
      <xsl:text> »</xsl:text>
    </xsl:if>
  </xsl:element>
</xsl:template>

<!-- Récupère un document à partir de <ref> -->
<xsl:template name="getdocfile">
  <xsl:param name="ref"/>
  <xsl:for-each select="$fichiers">
    <xsl:variable name="doc" select="document(concat($dir, '/', string(.)))"/>
    <xsl:if test="$doc/*/info/ref[text()=$ref]">
      <xsl:value-of select="concat($dir, '/', string(.))"/>
    </xsl:if>
  </xsl:for-each>
</xsl:template>


</xsl:stylesheet>
