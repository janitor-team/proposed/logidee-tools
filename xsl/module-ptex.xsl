<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:param name="selection" select="'all'"/>
<xsl:param name="trainer" select="'false'"/>

<xsl:output method="text" encoding="UTF-8"/>

<xsl:strip-space elements="title row table col"/>
<xsl:preserve-space elements="code"/>

<xsl:include href="charte.xsl"/>
<xsl:include href="tex.xsl"/>

<!-- The module -->
<xsl:template match="module">
<xsl:text>
\documentclass[11pt,presentation,</xsl:text>
<xsl:value-of select="$mylang"/>
<xsl:text>]{logidoc}

\begin{document}
</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\end{document}</xsl:text>
</xsl:template>

<!-- The slide show -->
<xsl:template match="slideshow">
<xsl:text>
\documentclass[11pt,presentation]{logidoc}

\begin{document}
</xsl:text>
  <xsl:apply-templates select="info|shortinfo"/>
  <xsl:for-each select="slide">
    <xsl:text>\page{</xsl:text>
    <xsl:apply-templates select="title"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:apply-templates select="."/>
  </xsl:for-each>
  <xsl:text>\end{document}</xsl:text>
</xsl:template>

<!-- The page -->
<xsl:template match="page">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:text>\page{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:choose>
    <xsl:when test="not(.//slide)">
      <xsl:call-template name="diapoauto"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select=".//slide"/>
    </xsl:otherwise>
  </xsl:choose>
  <!-- An image is visible by default, a block of code isn't -->
  <xsl:apply-templates select="
    ./section//image[@visible='true' or not(@visible)] | 
    ./section//code[@visible='true']" mode="singlediapo"/>
  <!-- No global apply-templates, very few things are of interest -->
  </xsl:if>
</xsl:template>

<!-- The <info> header -->
<xsl:template match="info|shortinfo">
  <xsl:text>\titrep{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
</xsl:template>

<!-- A slide -->
<xsl:template match="slide">
  <xsl:choose>
    <xsl:when test="image">
      <xsl:call-template name="begindiapo"/>
      <xsl:apply-templates select="image" mode="diapo"/> 
      <xsl:call-template name="enddiapo"/>
    </xsl:when>
    <xsl:when test="code">
      <xsl:apply-templates select="code" mode="singlediapo"/>
    </xsl:when>
    <xsl:when test="list">
      <xsl:call-template name="begindiapo"/>
      <xsl:apply-templates select="list" mode="diapo"/> 
      <xsl:call-template name="enddiapo"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="begindiapo"/>
      <xsl:apply-templates mode="diapo"/>
      <xsl:call-template name="enddiapo"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- An image as an independant slide -->
<xsl:template match="image" mode="singlediapo">
  <xsl:call-template name="begindiapo">
    <xsl:with-param name="titre" select="ancestor::page/title"/>
  </xsl:call-template>
  <xsl:text>\imagedia{</xsl:text>
    <xsl:value-of select="@src"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:call-template name="enddiapo"/>
</xsl:template>

<!-- A piece of code as an independant slide -->
<xsl:template match="code" mode="singlediapo">
  <xsl:call-template name="begindiapo">
    <xsl:with-param name="titre" select="ancestor::page/title"/>
  </xsl:call-template>
  <xsl:text>\begin{code}</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\end{code}&#10;</xsl:text>
  <xsl:call-template name="enddiapo"/>
</xsl:template>

</xsl:stylesheet>
