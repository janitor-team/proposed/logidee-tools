<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output encoding="UTF-8" 
            omit-xml-declaration="yes" 
	    indent="yes"
	    method="html"/>

<xsl:strip-space elements="title table row col"/>
<xsl:preserve-space elements="code"/>

<xsl:template match="section">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
    <xsl:apply-templates />
  </xsl:if>
</xsl:template>

<!-- Notes (with icon eventually) -->
<xsl:template match="note">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:if test="@trainer=$trainer or $trainer='true' or not(@trainer)">
  <div class="note">
  <table class="note" summary="note" width="80%">
  <tr class="note"><th class="cadre">
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Note</xsl:with-param>
  </xsl:call-template>
  </th></tr>
  <tr class="note"><td class="note">
  <xsl:choose>
    <xsl:when test="@icon">
      <xsl:element name="img">
        <xsl:attribute name="src">
  	<xsl:call-template name="icone">
  	  <xsl:with-param name="output">html</xsl:with-param>
  	</xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="class">icone</xsl:attribute>
      </xsl:element>
    </xsl:when>
    <xsl:when test="@trainer='true'">
      <xsl:element name="img">
        <xsl:attribute name="src">
  	<xsl:call-template name="icone">
  	  <xsl:with-param name="output">html</xsl:with-param>
  	  <xsl:with-param name="nom">trainer</xsl:with-param>
  	</xsl:call-template>
        </xsl:attribute>
        <xsl:attribute name="class">icone</xsl:attribute>
      </xsl:element>
    </xsl:when>
  </xsl:choose>
  <xsl:apply-templates/>
  </td></tr>
  </table>
  </div>
  </xsl:if>
  </xsl:if>
</xsl:template>

<!-- Exercise question (with icon eventually) -->
<xsl:template match="exercise">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <div class="question">
  <table class="question" summary="exercise" width="80%">
  <tr class="question"><th class="cadre">
  <xsl:call-template name="gettext">
    <xsl:with-param name="id">Exercise</xsl:with-param>
  </xsl:call-template>
  <xsl:text> </xsl:text>
  <xsl:number level="any" count="exercise" format="1"/>
  </th></tr>
  <tr class="question"><td class="question">
  <xsl:if test="@icon">
    <xsl:element name="img">
      <xsl:attribute name="src">
        <xsl:call-template name="icone">
	  <xsl:with-param name="output">html</xsl:with-param>
	</xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="class">icone</xsl:attribute>
    </xsl:element>
  </xsl:if>
  <xsl:apply-templates select="question" mode="question"/>
  <xsl:if test="answer">
    <div class="renvoi">
      <xsl:element name="a">
	<xsl:attribute name="href">
	  <xsl:text>exercises.html#exo</xsl:text>
	  <xsl:number level="any" count="exercise" format="1"/>
	</xsl:attribute>
	<xsl:call-template name="gettext">
	  <xsl:with-param name="id">solution_exercise</xsl:with-param>
	</xsl:call-template>
      </xsl:element>
    </div>
  </xsl:if>
  </td></tr>
  </table>
  </div>
  </xsl:if>
</xsl:template>

<!-- Paragraph of question -->
<xsl:template match="para" mode="question">
  <xsl:apply-templates/>
</xsl:template>

<!-- Paragraph (with icon eventually) -->
<xsl:template match="para">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <p class="para">
  <xsl:if test="@icon">
    <xsl:element name="img">
      <xsl:attribute name="src">
        <xsl:call-template name="icone">
	  <xsl:with-param name="output">html</xsl:with-param>
	</xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="class">icone</xsl:attribute>
    </xsl:element>
  </xsl:if>
  <xsl:apply-templates/>
  </p>
  </xsl:if>
</xsl:template>

<!-- Other "beautify" elements -->
<xsl:template match="title">
  <xsl:variable name="num" select="count(ancestor::section)+1"/>
  <xsl:choose>
    <!-- A section with a named link -->
    <xsl:when test="$num=2">
      <xsl:element name="h{$num}">
	<xsl:element name="a">
	  <xsl:attribute name="name">
	    <xsl:text>section</xsl:text>
	    <xsl:value-of select="count(parent::section/preceding-sibling::section)+1"/>
	  </xsl:attribute>
	  <xsl:apply-templates/>
	</xsl:element>
      </xsl:element>
    </xsl:when>
    <!-- A title -->
    <xsl:otherwise>
      <xsl:element name="h{$num}">
        <xsl:apply-templates/>
      </xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Some output for elements of <info> -->
<xsl:template match="objectives">
  <ul>
    <xsl:apply-templates/>
  </ul>
</xsl:template>

<!-- Element for page make-up -->
<xsl:template match="code">
  <pre class="code"><xsl:apply-templates/></pre>
</xsl:template>

<xsl:template match="list">
  <ul>
    <xsl:apply-templates/>
  </ul>
</xsl:template>

<xsl:template match="item">
  <li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="em">
  <em><xsl:apply-templates/></em>
</xsl:template>

<xsl:template match="menu">
  <span class="menu"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="cmd">
  <span class="cmd"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="file">
  <span class="fichier"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="url">
  <xsl:element name="a">
    <xsl:attribute name="href">
      <xsl:value-of select="@href"/>
    </xsl:attribute>
    <xsl:attribute name="class">lien</xsl:attribute>
    <xsl:choose>
      <xsl:when test="string(.)=''">
	<xsl:value-of select="@href"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:element>
</xsl:template>

<xsl:template match="email">
  <xsl:element name="a">
    <xsl:attribute name="href">
      <xsl:text>mailto:</xsl:text>
      <xsl:apply-templates/>
    </xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<!-- Images -->
<xsl:template match="image">
  <div class="image">
  <table class="image">
  <tr><td>
  <xsl:element name="img">
    <xsl:attribute name="src">
      <xsl:call-template name="basename">
	<xsl:with-param name="nom">
	  <xsl:call-template name="substext">
	    <xsl:with-param name="nom" select="@src"/>
	    <xsl:with-param name="ext" select="'png'"/>
	  </xsl:call-template>
	</xsl:with-param>
      </xsl:call-template>
    </xsl:attribute>
    <xsl:attribute name="class">image</xsl:attribute>
  </xsl:element>
  </td></tr>
  <xsl:if test="legend|caption">
    <tr><th>
      <xsl:apply-templates/>
    </th></tr>
  </xsl:if>
  </table>
  </div>
</xsl:template>

<!-- Tables -->
<xsl:template match="table">
  <div class="tableau">
  <xsl:element name="table">
    <xsl:attribute name="border">
      <xsl:choose>
        <xsl:when test="@border=1 or not(@border)">
	  <xsl:text>1</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>0</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="class">
      <xsl:text>tableau</xsl:text>
    </xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
  </div>
</xsl:template>

<xsl:template match="row">
  <tr class="tableau"><xsl:apply-templates/></tr>
  <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template name="align">
  <xsl:variable name="numcol" select="count(preceding-sibling::col)+1"/>
  <xsl:attribute name="align">
    <xsl:choose>
      <!-- Use the given alignment -->
      <xsl:when test="@align">
        <xsl:choose>
	  <xsl:when test="@align='c'">
	    <xsl:text>center</xsl:text>
	  </xsl:when>
	  <xsl:when test="@align='l'">
	    <xsl:text>left</xsl:text>
	  </xsl:when>
	  <xsl:when test="@align='r'">
	    <xsl:text>right</xsl:text>
	  </xsl:when>
	</xsl:choose>
      </xsl:when>
      <!-- Use the alignement of the first row -->
      <xsl:when test="ancestor::table/row[1]/col[$numcol]/@align">
	<xsl:choose>
	  <xsl:when test="ancestor::table/row[1]/col[$numcol]/@align='c'">
	    <xsl:text>center</xsl:text>
	  </xsl:when>
	  <xsl:when test="ancestor::table/row[1]/col[$numcol]/@align='l'">
	    <xsl:text>left</xsl:text>
	  </xsl:when>
	  <xsl:when test="ancestor::table/row[1]/col[$numcol]/@align='r'">
	    <xsl:text>right</xsl:text>
	  </xsl:when>
	</xsl:choose>
      </xsl:when>
      <!-- Otherwise centered by default -->
      <xsl:otherwise>
        <xsl:text>center</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
</xsl:template>

<xsl:template match="col">
  <xsl:choose>
    <xsl:when test="@head=1">
      <xsl:element name="th">
        <xsl:attribute name="class">tableau</xsl:attribute>
        <xsl:call-template name="align"/>
        <xsl:apply-templates/>
      </xsl:element>
    </xsl:when>
    <xsl:otherwise>
      <xsl:element name="td">
        <xsl:attribute name="class">tableau</xsl:attribute>
        <xsl:call-template name="align"/>
        <xsl:apply-templates/>
      </xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Math notation -->
<xsl:template match="math">
  <xsl:if test="@notation='html'">
    <xsl:apply-templates/>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
