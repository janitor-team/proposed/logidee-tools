<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Raphaël Hertzog, Logidée 2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:param name="cycle" select="'true'"/>
<xsl:param name="charte" select="'default'"/>
<xsl:param name="lang" select="'en'"/>

<xsl:variable name="chartedir" select="'../charte/'"/>
<xsl:variable name="chartedata" 
     select="document(concat($chartedir, $charte, '/charte.xml'))/charte"/>
<xsl:variable name="messages" 
     select="document(concat($chartedir, $charte, '/messages.xml'))/messages"/>
<xsl:variable name="mylang">
  <xsl:choose>
    <xsl:when test="contains($lang, 'en')">
      <xsl:text>en</xsl:text>
    </xsl:when>
    <xsl:when test="contains($lang, 'fr')">
      <xsl:text>fr</xsl:text>
    </xsl:when>
    <xsl:when test="contains($lang, 'de')">
      <xsl:text>de</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message>Language "<xsl:value-of select="$lang"/>" not officially supported. "en", "fr" and "de" are currently supported. You may get unexpected results.</xsl:message>
      <xsl:value-of select="$lang"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<!-- ## Macros to manage i18n ## -->

<xsl:template name="gettext">
  <xsl:param name="id" select="''"/>
  <xsl:param name="lang" select="$mylang"/>
  
  <xsl:value-of
   select="$messages/message[@msgid=$id]/translation[@lang=$lang]"/>

  <xsl:if test="not($messages/message[@msgid=$id]/translation[@lang=$lang])">
    <xsl:message>There's no translation for message '<xsl:value-of
    select="$id"/>' in language '<xsl:value-of select="$lang"/>' !</xsl:message>
  </xsl:if>
</xsl:template>

<!-- ## Macros to manage the graphical style (charte) ## -->

<!-- Find the filename associated with the icon -->
<xsl:template name="icone">
  <xsl:param name="nom" select="@icon"/>
  <xsl:param name="output" select="'tex'"/>
  <xsl:variable name="fichier"
   select="$chartedata/icons[@output=$output]/item[@name=$nom]/@file"/>
  <xsl:choose>
    <xsl:when test="string-length($fichier)">
      <xsl:value-of select="$fichier"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:choose>
        <xsl:when test="$output='html'">
	  <xsl:call-template name="substext">
            <xsl:with-param name="nom">
	      <xsl:value-of select="$nom"/>
	    </xsl:with-param>
	    <xsl:with-param name="ext">png</xsl:with-param>
          </xsl:call-template>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$nom"/>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Find the filename associated with the background -->
<xsl:template name="fond">
  <xsl:param name="nom" select="@background"/>
  <xsl:param name="output" select="'tex'"/>
  <xsl:variable name="fichier"
   select="$chartedata/backgrounds[@output=$output]/item[@name=$nom]/@file"/>
  <xsl:choose>
    <xsl:when test="string-length($fichier)">
      <xsl:value-of select="$fichier"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:choose>
        <xsl:when test="$output='html'">
	  <xsl:call-template name="substext">
            <xsl:with-param name="nom">
	      <xsl:value-of select="$nom"/>
	    </xsl:with-param>
	    <xsl:with-param name="ext">png</xsl:with-param>
          </xsl:call-template>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$nom"/>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Choose the image from the cycle -->
<xsl:template name="cycle">
  <!-- default value for tex output -->
  <xsl:param name="output" select="'tex'"/>
  <xsl:param name="avant">[</xsl:param>
  <xsl:param name="apres">]</xsl:param>
  <xsl:if test="$cycle='true'">
    <xsl:variable name="num" select="count(preceding::page) mod
     count($chartedata/cycle[@output=$output]/item)"/>
    <xsl:value-of select="$avant"/>
    <xsl:value-of 
     select="$chartedata/cycle[@output=$output]/item[$num+1]/@file"/>
    <xsl:value-of select="$apres"/>
  </xsl:if>
</xsl:template>

<!-- Change the extension of a filename -->
<xsl:template name="substext">
  <xsl:param name="nom"/>
  <xsl:param name="ext" select="'png'"/>
  <xsl:choose>
    <xsl:when test="contains($nom, '.ps')">
      <xsl:value-of select="concat(substring-before($nom, '.ps'), '.', $ext)"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="concat(substring-before($nom, '.eps'), '.', $ext)"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Extract the basename of the filename -->
<xsl:template name="basename">
  <xsl:param name="nom" select="''"/>
  <xsl:choose>
    <xsl:when test="contains($nom, '/')">
      <xsl:call-template name="basename">
        <xsl:with-param name="nom" select="substring-after($nom, '/')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="string($nom)"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Insert the HTML footer -->
<xsl:template name="noticehtml">
  <xsl:copy-of select="document(concat($chartedir, $charte, '/footer.html.', $lang))"/>
</xsl:template>
    
</xsl:stylesheet>
