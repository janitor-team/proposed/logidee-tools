<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="module-ptex.xsl"/> 

<xsl:output method="text" encoding="UTF-8"/>

<xsl:template match="theme">
<xsl:text>
\documentclass[11pt,presentation,</xsl:text>
<xsl:value-of select="$mylang"/>
<xsl:text>]{logidoc}

\begin{document}
</xsl:text>
  <xsl:apply-templates select="info" mode="theme"/>
  <xsl:apply-templates select="module"/>
  <xsl:text>\end{document}</xsl:text>
</xsl:template>

<xsl:template match="module">
  <xsl:apply-templates select="info"/>
  <xsl:apply-templates select="page"/>
</xsl:template>

<xsl:template match="info" mode="theme">
  <xsl:text>\titrep{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
</xsl:template>

<xsl:template match="info">
  <xsl:text>\titres{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>

