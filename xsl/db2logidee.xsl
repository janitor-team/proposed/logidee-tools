<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output encoding="UTF-8" method="xml" standalone="no"
	    doctype-system="../dtd/module.dtd"
	    doctype-public="-//Logidee//DTD logidee-tools V1.2//EN"
	    cdata-section-elements="code" />

<!-- The possible roots of docbook documents -->
<xsl:template match="book">
  <xsl:choose>
    <xsl:when test="part">
      <formation>
      <xsl:call-template name="buildinfo"/>
      <xsl:apply-templates select="part"/>
      </formation>
    </xsl:when>
    <xsl:otherwise>
      <theme>
      <xsl:call-template name="buildinfo"/>
      <xsl:apply-templates select="chapter|article"/>
      </theme>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="part">
  <theme>
    <xsl:call-template name="buildinfo"/>
    <xsl:apply-templates select="chapter"/>
  </theme>
</xsl:template>

<xsl:template match="chapter|article">
  <module>
    <xsl:call-template name="buildinfo"/>
    <xsl:apply-templates select="sect1|section|simplesect" mode="start"/>
  </module>
</xsl:template>

<!-- Titles -->
<xsl:template match="title">
  <title><xsl:apply-templates/></title>
</xsl:template>

<!-- Build an <info> header -->
<xsl:template name="buildinfo">
  <info>
    <xsl:text>&#10;</xsl:text>
    <titre><xsl:value-of select="(docinfo|bookinfo|artheader)/title"/></titre>
    <xsl:text>&#10;</xsl:text>
    <ref>replace_with_id</ref>
    <xsl:text>&#10;</xsl:text>
    <description>
    <xsl:value-of select="(docinfo|bookinfo|artheader)/abstract"/>
    </description>
    <xsl:text>&#10;</xsl:text>
    <objectives>
    <xsl:text>&#10;</xsl:text>
      <item>Put the description of the goal of this module</item>
    <xsl:text>&#10;</xsl:text>
    </objectives>
    <xsl:text>&#10;</xsl:text>
    <ratio value="th/pr"/>
    <xsl:text>&#10;</xsl:text>
    <duration value="" unit=""/>
    <xsl:text>&#10;</xsl:text>
    <xsl:element name="version">
      <xsl:attribute name="number">
        <xsl:value-of select="(docinfo|bookinfo|artheader)//revision/revnumber[position()=last()]"/>
      </xsl:attribute>
      <xsl:text>&#10;</xsl:text>
      <author>
      <xsl:variable name="_auteurs">
      <xsl:for-each select="(docinfo|bookinfo|artheader)//author">
        <xsl:value-of select="firstname"/> 
	<xsl:text> </xsl:text>
        <xsl:value-of select="surname"/> 
	<xsl:text>, </xsl:text>
      </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="normalize-space(substring($_auteurs,1,string-length($_auteurs)-2))"/>
      </author>
      <xsl:text>&#10;</xsl:text>
      <comment>
        <xsl:value-of select="(docinfo|bookinfo|artheader)//revision/revremark[position()=last()]"/>
      </comment>
      <xsl:text>&#10;</xsl:text>
    </xsl:element>
    <xsl:text>&#10;</xsl:text>
    <level value="/"/>
    <xsl:text>&#10;</xsl:text>
    <state finished="false" proofread="false"/>
    <xsl:text>&#10;</xsl:text>
  </info>
</xsl:template>

<!-- Sections and other elements that structure the document -->
<xsl:template match="simplesect|sect1|sect2|sect3|sect4|sect5|section|chapter">
  <section>
  <xsl:apply-templates/>
  </section>
</xsl:template>

<xsl:template match="simplesect|sect1|sect2|sect3|sect4|sect5|section|chapter"
	      mode="start">
  <page>
  <xsl:apply-templates/>
  </page>
</xsl:template>

<xsl:template match="part">
  
</xsl:template>

<xsl:template match="para|simpara|formalpara">
  <para>
  <xsl:apply-templates/>
  </para>
</xsl:template>

<xsl:template match="footnote">
  <xsl:message>Footnotes are not supported (converted to note) !</xsl:message>
  <note>
  <xsl:apply-templates/>
  </note>
</xsl:template>

<xsl:template match="note|warning|caution|important|tip|highlights">
  <xsl:element name="note">
    <xsl:if test="name(.)='warning' or name(.)='caution'
		  or name(.)='important'">
      <xsl:attribute name="icon">
        <xsl:text>attention</xsl:text>
      </xsl:attribute>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<!-- Elements for page make-up -->
<xsl:template match="itemizedlist|orderedlist|simplelist">
  <list>
  <xsl:apply-templates/>
  </list>
</xsl:template>

<xsl:template match="listitem|member">
  <item>
  <xsl:apply-templates/>
  </item>
</xsl:template>

<xsl:template match="filename">
  <file><xsl:apply-templates/></file>
</xsl:template>

<xsl:template match="abbrev|acronym|command|literal">
  <cmd><xsl:apply-templates/></cmd>
</xsl:template>

<xsl:template match="programlisting|literallayout">
  <code>
  <xsl:apply-templates/>
  </code>
</xsl:template>

<xsl:template match="ulink">
  <xsl:element name="url">
    <xsl:attribute name="href">
      <xsl:value-of select="@url"/>
    </xsl:attribute>
    <xsl:if test="@url!=text()">
      <xsl:value-of select="text()"/>
    </xsl:if>
  </xsl:element>
</xsl:template>

<xsl:template match="emphasis">
  <em>
  <xsl:apply-templates/>
  </em>
</xsl:template>

<xsl:template match="blockquote">
  <code>
  <xsl:apply-templates mode="blockquote"/>
  </code>
</xsl:template>

<!-- Images -->
<xsl:template match="graphic">
  <xsl:if test="contains(@fileref,'.eps')">
    <xsl:message>The image <xsl:value-of select="@fileref"/> is not in .eps. You'll have to change that if you want the XML file to work with logidee-tools.</xsl:message>
  </xsl:if>
  <xsl:element name="image">
    <xsl:attribute name="src">
      <xsl:value-of select="@fileref"/>
    </xsl:attribute>
  </xsl:element>
</xsl:template>

<!-- Tables -->
<xsl:template match="table|informaltable">
  <table>
  <xsl:apply-templates select="thead|tbody|tgroup"/>
  </table>
</xsl:template>

<xsl:template match="row">
  <row>
  <xsl:apply-templates/>
  </row>
</xsl:template>

<xsl:template match="entry">
  <xsl:element name="col">
    <xsl:if test="../../self::thead">
      <xsl:attribute name="head">1</xsl:attribute>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
