<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:import href="theme-tex.xsl"/> 

<xsl:output method="text" encoding="UTF-8"/>

<xsl:template match="formation">
  <xsl:call-template name="begindoc"/>
  <xsl:apply-templates select="info" mode="formation"/>
  <xsl:text>\sommaire&#10;</xsl:text>
  <xsl:apply-templates select="theme"/>
  <xsl:call-template name="enddoc"/>
</xsl:template>

<xsl:template match="theme">
  <xsl:apply-templates select="info" mode="theme"/>
  <xsl:apply-templates select="module"/>
</xsl:template>

<xsl:template match="info" mode="formation">
  <xsl:text>\titrep{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;\pagenotice{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
</xsl:template>

<xsl:template match="info" mode="theme">
  <xsl:text>\titres{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:apply-templates select="description"/>
  <xsl:apply-templates select="objectives"/>
</xsl:template>

<xsl:template match="info">
  <xsl:text>\titret{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:apply-templates select="description"/>
  <xsl:apply-templates select="objectives"/>
</xsl:template>

</xsl:stylesheet>

